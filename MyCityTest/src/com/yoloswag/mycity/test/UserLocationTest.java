package com.yoloswag.mycity.test;

import junit.framework.TestCase;

import org.jivesoftware.smackx.packet.Time;

import android.location.Location;

import com.yoloswag.models.MyCityUser;

public class UserLocationTest extends TestCase {

	public void testUserLocation() {
		Location loc = new Location("");
		loc.setLatitude(1);
		loc.setLongitude(1);
		
		MyCityUser u = new MyCityUser(loc, "lol@gmail.com", "lol");
		assertTrue(u.getLocation().equals(loc));
		assertTrue(u.getJid().equals("lol@gmail.com"));
		assertEquals(u.getGeoPoint().getLatitudeE6(), 1/(1E6), 1E6);
		assertEquals(u.getGeoPoint().getLongitudeE6(), 1/(1E6), 1E6);
	}

}
