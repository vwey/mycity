package com.yoloswag.mycity.test;

import junit.framework.TestCase;

import org.jivesoftware.smack.XMPPConnection;

import android.location.Location;
import android.util.Log;

import com.yoloswag.models.Message;
import com.yoloswag.models.MyCityUser;
import com.yoloswag.mycity.MySmack;
import com.yoloswag.services.ChatPacketService;
import com.yoloswag.services.ChatService;
import com.yoloswag.services.SubscriptionEnum;
import com.yoloswag.services.interfaces.YoloswagListener;
import com.yoloswag.services.interfaces.YoloswagService;

public class ChatPacketServiceTest extends TestCase {/*
	final static String TRKPT_GOOD = "<trkpt lat=\"46.57608333\" lon=\"8.89241667\">" +
	"<ele>2376</ele>" +
	"<time>2007-10-14T10:09:57Z</time>" +
	"</trkpt>";
	final static String MISSPELL_TRKPT = "<trkt lat=\"46.57608333\" lon=\"8.89241667\">" +
	"<ele>2376</ele>" +
	"<time>2007-10-14T10:09:57Z</time>" +
	"</trkpt>";
    public void setup(){

    	loc.setLatitude(1);
    	loc.setLongitude(1);

    }
	static Location loc = new Location("");
	static MyCityUser user1 = new MyCityUser(loc, "ucsdteamyoloswag@gmail.com", "yolo swag");
	static MyCityUser user2 = new MyCityUser(loc, "experimentpants@gmail.com", "experiment pants");
	static Object[][] testCases = {
			//Input					//Output			//Expected Handler
//		{	"",						"", 				"CHAT"	},
		{   "Hi",					new Message(user2, user1, "Hi", null), 				"CHAT"	},
		{	loc,				user2,												"GPS"	},
		{	MISSPELL_TRKPT,			new Message(user2, user1, MISSPELL_TRKPT, null),	"CHAT"	}
	};
	
	
	@SuppressWarnings("static-access") //yolo
	public void testPacketService()
	{
			XMPPConnection connection = MySmack.connectToGtalk("ucsdteamyoloswag@gmail.com", "yoloswag123");
			XMPPConnection connection2 = MySmack.connectToGtalk("experimentpants@gmail.com", "2w3e2w3e");
			loc.setLatitude(1);
			loc.setLongitude(1);
			ChatPacketService packetService = new ChatPacketService(connection);
			ChatPacketService packetService2 = new ChatPacketService(connection2);
			ChatListener cl 			= new ChatListener(packetService);
			GPSListener gl 				= new GPSListener(packetService);

			for(int ii = 0; ii < testCases.length; ii++)
			{
				packetService2.sendMessage((String)(testCases[ii][0]), connection.getUser());
				//Wait for packet
				try
				{
					Thread.currentThread().sleep(3000*5);
					packetService.update();
					packetService.updateListeners();
				} catch(InterruptedException ex) {
					//yolo
				}
				if( testCases[ii][2].equals("CHAT") )
				{
					Log.d("PacketServiceTest","Message received was \"" + cl.message + "\"");
					if( !testCases[ii][1].equals(cl.message.getBody())) // Look at ChatListener
					{
						fail("Failed on test case " + ii + ": Message received was not message sent!");
					}
				}
				else if( testCases[ii][2].equals("GPS") )
				{
					Log.d("PacketServiceTest","Message received was \"" + gl.user.getLocation() + "\"");
					if( !testCases[ii][1].equals(gl.user.getLocation())) // Look at GPSListener
					{
						fail("Failed on test case " + ii + ": Message received was not message sent!");
					}
				}
				else
				{
					fail("Unsupported format.");
				}
			}
	}
}

class ChatListener implements YoloswagListener
{
	SubscriptionEnum subscrType;
	Message message;
	
	public ChatListener(ChatPacketService ps)
	{
		ps.registerListener(this);
		setSubscriptionType(null);
	}

	@Override
	public void update(Object data, Class<? extends YoloswagService> service) {
		this.message = (Message) data;
	}

	@Override
	public Object getSubscriptionType() {
		return subscrType;
	}

	@Override
	public void setSubscriptionType(SubscriptionEnum type) {
		subscrType = SubscriptionEnum.CHAT;
	}
}

class GPSListener implements YoloswagListener
{
	SubscriptionEnum subscrType;
	MyCityUser user;
	
	public GPSListener(ChatPacketService ps)
	{
		ps.registerListener(this);
		setSubscriptionType(null);
	}

	@Override
	public void update(Object data, Class<? extends YoloswagService> service) {
		this.user = (MyCityUser) data;
	}

	@Override
	public Object getSubscriptionType() {
		return subscrType;
	}

	@Override
	public void setSubscriptionType(SubscriptionEnum type) {
		subscrType = SubscriptionEnum.GPS;
	}*/
}
