package com.yoloswag.mycity.test;

import org.jivesoftware.smackx.packet.Time;

import android.location.Location;

import com.yoloswag.models.MyCityUser;
import com.yoloswag.util.CoordParser;

import junit.framework.TestCase;

public class CoordParserTest extends TestCase {
	
	private String EMAIL = "john@gmail.com";
	private String NAME = "John Doe";
	private String TRKPT_GOOD = "<trkpt lat=\"46.57608333\" lon=\"8.89241667\">" +
								"<ele>2376</ele>" +
								"<time>2007-10-14T10:09:57Z</time>" +
								"</trkpt>";
	private String NO_TRKPT = "This has no track point data";
	private String MISSPELL_TRKPT = "<trkt lat=\"46.57608333\" lon=\"8.89241667\">" +
									"<ele>2376</ele>" +
									"<time>2007-10-14T10:09:57Z</time>" +
									"</trkpt>";
	
	private String MISSPELL_LAT = "<trkpt lt=\"46.57608333\" lon=\"8.89241667\">" +
			"<ele>2376</ele>" +
			"<time>2007-10-14T10:09:57Z</time>" +
			"</trkpt>";
	private String MISSPELL_LON = "<trkpt lat=\"46.57608333\" ln=\"8.89241667\">" +
			"<ele>2376</ele>" +
			"<time>2007-10-14T10:09:57Z</time>" +
			"</trkpt>";
	private String MISSPELL_TIME =	"<trkpt lat=\"46.57608333\" lon=\"8.89241667\">" +
									"<ele>2376</ele>" +
									"<tim>2007-10-14T10:09:57Z</time>" +
									"</trkpt>";

	private String NO_LAT = "<trkpt lon=\"8.89241667\">" +
			"<ele>2376</ele>" +
			"<time>2007-10-14T10:09:57Z</time>" +
			"</trkpt>";
	private String NO_LON = "<trkpt lat=\"46.57608333\"" +
			"<ele>2376</ele>" +
			"<time>2007-10-14T10:09:57Z</time>" +
			"</trkpt>";
	private String NO_TIME = "<trkpt lat=\"46.57608333\" lon=\"8.89241667\">" +
			"<ele>2376</ele>" +
			"</trkpt>";
	
	private String NO_QUOTES_LAT = "<trkpt lat=46.57608333 lon=\"8.89241667\">" +
			"<ele>2376</ele>" +
			"<time>2007-10-14T10:09:57Z</time>" +
			"</trkpt>";
	private String NO_QUOTES_LON = "<trkpt lat=\"46.57608333\" lon=8.89241667>" +
			"<ele>2376</ele>" +
			"<time>2007-10-14T10:09:57Z</time>" +
			"</trkpt>";
	private String MISSING_QUOTES = "<trkpt lat=\"46.57608333 lon=\"8.89241667\">" +
			"<ele>2376</ele>" +
			"<time>2007-10-14T10:09:57Z</time>" +
			"</trkpt>";
	
	public void testCoordParser(){
		Location trackGood= CoordParser.parse(TRKPT_GOOD);
		assertEquals((double) 46.57608333, trackGood.getLatitude());
		assertEquals((double) 8.89241667, trackGood.getLongitude());
	}
	
	public void testCoordParserNoTrkpt(){
	Location noTrack = CoordParser.parse(NO_TRKPT);
		assertEquals(null, noTrack);
	}
	
	public void testCoordParserMisspellTrkpt(){
		Location misSpell = CoordParser.parse(MISSPELL_TRKPT);
		assertEquals(null, misSpell);
	}
	
	public void testCoordParserMisspellLat(){
		Location misSpell = CoordParser.parse(MISSPELL_LAT);
		assertEquals(null, misSpell);
	}
	
	public void testCoordParserMisspellLon(){
		Location misSpell  = CoordParser.parse(MISSPELL_LON);
		assertEquals(null, misSpell);
	}
	
	public void testCoordParserMisspellTime(){
		Location misSpell = CoordParser.parse(MISSPELL_TIME);
		assertEquals((double) 46.57608333, misSpell.getLatitude());
		assertEquals((double) 8.89241667, misSpell.getLongitude());
	}
	
	public void testCoordParserNoLat(){
		Location noLat = CoordParser.parse(NO_LAT);
		assertEquals(null, noLat);
	}
	
	public void testCoordParserNoLon(){
		Location noLon = CoordParser.parse(NO_LON);
		assertEquals(null, noLon);
	}

	public void testCoordParserNoTime(){
		Location noTime = CoordParser.parse(NO_TIME);
		assertEquals((double) 46.57608333, noTime.getLatitude());
		assertEquals((double) 8.89241667, noTime.getLongitude());
	}
	
	public void testCoordParserNoQuotesLat(){
		Location noQuote = CoordParser.parse(NO_QUOTES_LAT);
		assertEquals(null, noQuote);
	}
	
	public void testCoordParserNoQuotesLon(){
		Location noQuote = CoordParser.parse(NO_QUOTES_LON);
		assertEquals(null, noQuote);
	}
	
	public void testCoordParserMissingQuotes(){
		Location miss = CoordParser.parse(MISSING_QUOTES);
		assertEquals(null, miss);
	}
}
