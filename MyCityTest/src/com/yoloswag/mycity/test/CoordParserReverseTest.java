package com.yoloswag.mycity.test;

import junit.framework.TestCase;
import android.location.Location;

import com.yoloswag.models.MyCityUser;
import com.yoloswag.util.CoordParserReverse;

import org.jivesoftware.smackx.packet.Time;


public class CoordParserReverseTest extends TestCase{
	
	private double latitude = 32.88894503;
	private double longitude = -117.77628393;
	private Time time;
	Location l;
	private MyCityUser USER_GOOD;
	private String[] TRKPT_GOOD = {"<trkpt lat=\"32.88894503\" lon=\"-117.77628393\">" +
			"</trkpt>", "john@doe.com"};
	
	public void setUp(){
		l = new Location("");
		l.setLatitude(latitude);
		l.setLongitude(longitude);
		time = new Time();
		time.setUtc("2007-10-14T10:09:57");
	}
	
	public void testCoordParserReverse(){
		USER_GOOD = new MyCityUser(l, "john@doe.com", "John Doe");
		assertEquals(TRKPT_GOOD[0], CoordParserReverse.parse(USER_GOOD)[0]);
		assertEquals(TRKPT_GOOD[1], CoordParserReverse.parse(USER_GOOD)[1]);
	}
	
	public void testCoordParserReverseNull(){
		USER_GOOD = null;
		assertEquals(null, CoordParserReverse.parse(USER_GOOD));
	}
}
