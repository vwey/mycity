package com.yoloswag.mycity.test;

import org.jivesoftware.smack.XMPPConnection;

import android.location.Location;
import android.util.Log;

import com.yoloswag.models.MyCityUser;
import com.yoloswag.mycity.MySmack;
import com.yoloswag.services.ChatPacketService;
import com.yoloswag.services.UserService;

import junit.framework.TestCase;

public class FindUserTest extends TestCase {
	

	static Location loc = new Location("");

	
    public void setup(){
    	//loc.setLatitude();
    	//loc.setLongitude(1);
    }
    
    public void testFindUser(){
    	XMPPConnection connection = MySmack.connectToGtalk("ucsdteamyoloswag@gmail.com", "yoloswag123");
    	MyCityUser user1 = new MyCityUser(null, "simonbui723@gmail.com", "simonbui723@gmail.com");
 		MyCityUser user2 = new MyCityUser(null, "ucsdteamyoloswag@gmail.com", "ucsdteamyoloswag@gmail.com");
 		MyCityUser user3 = new MyCityUser(null, "doughboy@gmail.com", "doughboy@gmail.com");
 		MyCityUser user4 = new MyCityUser(null, "experimentpants@gmail.com", "experimentpants@gmail.com");
    	UserService useS = new UserService(connection, user2);
    	useS.update();


        assertEquals(useS.findUser("simonbui723@gmail.com"),user1);
        assertEquals(useS.findUser("doughboy@gmail.com"), null);
        assertEquals(useS.findUser("experimentpants@gmail.com"), user4);
        assertEquals(useS.findUser("simonbui722@gmail.com"), null);

    }
}
