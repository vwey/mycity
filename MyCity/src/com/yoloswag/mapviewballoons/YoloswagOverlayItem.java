package com.yoloswag.mapviewballoons;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.OverlayItem;

public class YoloswagOverlayItem extends OverlayItem {

	public String recipient;
	
	public YoloswagOverlayItem(GeoPoint location, String title, String subtitle, String recipient) {
		super(location, title, subtitle);
		this.recipient = recipient;
	}

}
