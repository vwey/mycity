package com.yoloswag.map;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.os.Handler;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.ItemizedOverlay;
import com.google.android.maps.MapController;
import com.google.android.maps.MapView;
import com.google.android.maps.Overlay;
import com.google.android.maps.OverlayItem;
import com.yoloswag.chat.ChatTypeEnum;
import com.yoloswag.chat.XMPPChatActivity;
import com.yoloswag.mapviewballoons.BalloonOverlayView;
import com.yoloswag.mycity.R;
import com.yoloswag.mycity.R.id;

public class MyOverlay extends ItemizedOverlay<OverlayItem> 
{
	private static final long BALLOON_INFLATION_TIME = 300;
	private static Handler handler = new Handler();
	private static boolean isInflating = false;
	
	private ArrayList<OverlayItem> buddies = new ArrayList<OverlayItem>();
	private BalloonOverlayView<OverlayItem> balloonView;
	private Context mContext;
	private MapView mapView;
	private View closeRegion;
	private View chatButton;
	private int viewOffset;
	private OverlayItem currentFocusedItem;
	private boolean userChat;

	final MapController mc;
	private Location currentLocation;
  
  public MyOverlay(Drawable defaultMarker, MapView mapView, boolean chat) {
	  super(boundCenter(defaultMarker));
      boundCenter(defaultMarker);
	  this.mapView = mapView;
	  viewOffset = 0;
	  mc = mapView.getController();
      mContext = mapView.getContext();
      userChat = chat;
  }
  
  @Override
  protected OverlayItem createItem(int i) 
  {
      return buddies.get(i);
  }
  
  @Override
  public int size() 
  {
      return buddies.size();
  }
  
  public int getBalloonBottomOffset() {
		return viewOffset;
	}
  
  public void addOverlay(OverlayItem overlay) 
  {
      buddies.add(overlay);
      populate();
  }
	
	/**
	 * Override this method to perform actions upon an item being tapped before 
	 * its balloon is displayed.
	 * 
	 * @param index - The index of the item tapped.
	 */
	protected void onBalloonOpen(int index) {}
	
	/* (non-Javadoc)
	 * @see com.google.android.maps.ItemizedOverlay#onTap(int)
	 */
	@Override
	//protected final boolean onTap(int index) {
	public final boolean onTap(int index) {
		
		handler.removeCallbacks(finishBalloonInflation);
		isInflating = true;
		handler.postDelayed(finishBalloonInflation, BALLOON_INFLATION_TIME);
		
		currentFocusedItem = createItem(index);
		setLastFocusedIndex(index);
		
		onBalloonOpen(index);
		createAndDisplayBalloonOverlay(currentFocusedItem.getSnippet());
		
		return true;
	}
		
	/**
	 * Creates the balloon view. Override to create a sub-classed view that
	 * can populate additional sub-views.
	 */
	protected BalloonOverlayView<OverlayItem> createBalloonOverlayView() {
		return new BalloonOverlayView<OverlayItem>(getMapView().getContext(), getBalloonBottomOffset());
	}
	
	/**
	 * Expose map view to subclasses.
	 * Helps with creation of balloon views. 
	 */
	protected MapView getMapView() {
		return mapView;
	}
	
	/**
	 * Makes the balloon the topmost item by calling View.bringToFront().
	 */
	public void bringBalloonToFront() {
		if (balloonView != null) {
			balloonView.bringToFront();
		}
	}

	/**
	 * Sets the visibility of this overlay's balloon view to GONE and unfocus the item. 
	 */
	public void hideBalloon() {
		if (balloonView != null) {
			balloonView.setVisibility(View.GONE);
		}
		currentFocusedItem = null;
	}
	
	/**
	 * Hides the balloon view for any other BalloonItemizedOverlay instances
	 * that might be present on the MapView.
	 * 
	 * @param overlays - list of overlays (including this) on the MapView.
	 */
	private void hideOtherBalloons(List<Overlay> overlays) {
		
		for (Overlay overlay : overlays) {
			if (overlay instanceof MyOverlay && overlay != this) {
				((MyOverlay) overlay).hideBalloon();
			}
		}
		
	}
	
	public void hideAllBalloons() {
		if (!isInflating) {
			List<Overlay> mapOverlays = mapView.getOverlays();
			if (mapOverlays.size() > 1) {
				hideOtherBalloons(mapOverlays);
			}
			hideBalloon();
		}
	}
	
	/* (non-Javadoc)
	 * @see com.google.android.maps.ItemizedOverlay#getFocus()
	 */
	@Override
	public OverlayItem getFocus() {
		return currentFocusedItem;
	}
	
	public void setCurrentLocation(Location loc){
		this.currentLocation = loc;
	}
	
	/* (non-Javadoc)
	 * @see com.google.android.maps.ItemizedOverlay#setFocus(Item)
	 */
	@Override
	public void setFocus(OverlayItem item) {
		super.setFocus(item);	
		currentFocusedItem = item;
		if (currentFocusedItem == null) {
			hideBalloon();
		} else {
			createAndDisplayBalloonOverlay(item.getSnippet());
		}	
	}
	
	/**
	 * Creates and displays the balloon overlay by recycling the current 
	 * balloon or by inflating it from xml. 
	 * @return true if the balloon was recycled false otherwise 
	 */
	private boolean createAndDisplayBalloonOverlay(final String jid){
		boolean isRecycled;
		if (balloonView == null) {
			balloonView = createBalloonOverlayView();
			closeRegion = (View) balloonView.findViewById(R.id.balloon_close);
			if (closeRegion != null) {
				closeRegion.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						hideBalloon();	
					}
				});
			}
			
			chatButton = (View) balloonView.findViewById(R.id.chat_button);
			
			if(userChat)
			{
				if(chatButton != null) {
					chatButton.setOnClickListener(new OnClickListener() {
						@Override
						public void onClick(View v) {
							//TODO: need to send recipient info to xmppchat
							Intent chatPage = new Intent(mContext, XMPPChatActivity.class);
							chatPage.putExtra("jid", jid);
							chatPage.putExtra("convoType", ChatTypeEnum.SINGLE.toString());
							mContext.startActivity(chatPage);
						}
					});
				}
			}
			else
				chatButton.setVisibility(View.GONE);
			
			isRecycled = false;
		} else {
			isRecycled = true;
		}
	
		//TODO: figure out what all the code below this line is doing
		balloonView.setVisibility(View.GONE);
		
		List<Overlay> mapOverlays = mapView.getOverlays();
		if (mapOverlays.size() > 1) {
			hideOtherBalloons(mapOverlays);
		}
		
		if (currentFocusedItem != null)
			balloonView.setData(currentFocusedItem);
		
		GeoPoint point = currentFocusedItem.getPoint();
		MapView.LayoutParams params = new MapView.LayoutParams(
				LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT, point,
				MapView.LayoutParams.BOTTOM_CENTER);
		params.mode = MapView.LayoutParams.MODE_MAP;
		
		balloonView.setVisibility(View.VISIBLE);
		
		if (isRecycled) {
			balloonView.setLayoutParams(params);
		} else {
			mapView.addView(balloonView, params);
		}
		
		return isRecycled;
	}
	
	public static boolean isInflating() {
		return isInflating;
	}
	
	private static Runnable finishBalloonInflation = new Runnable() {
		public void run() {
			isInflating = false;
		}
	};
}
