package com.yoloswag.map;

//import java.util.ArrayList;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.Button;

import com.google.android.maps.MapActivity;
import com.yoloswag.mycity.GraffitiActivity;
import com.yoloswag.mycity.R;
import com.yoloswag.services.ServiceManager;

public class YoloswagMapActivity extends MapActivity {

	public Button btnUpdate;
	public Button findNear;
	public Button addGraffiti;
	public Button shoutBtn;
	
	private YoloswagMapController controller;
		
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);
        
        btnUpdate 	= (Button) findViewById(R.id.currLocBtn); 
        findNear 	= (Button) findViewById(R.id.findNearBtn);
        addGraffiti = (Button) findViewById(R.id.addGraffiti);
        shoutBtn	= (Button) findViewById(R.id.shoutBtn);
        
        controller 	= new YoloswagMapController(ServiceManager.getUserService(), this);
    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.activity_map, menu);
        return true;
    }

	@Override
	protected boolean isRouteDisplayed() {
		return false;
	}
	
	@Override
	protected void onResume() {
	    super.onResume();
	    controller.onResume();
	}
	
	@Override
	protected void onPause() {
	    super.onPause();
	    controller.onPause();
	}
	

	@Override
	public void onBackPressed() {
	}
	
}