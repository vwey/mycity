package com.yoloswag.map;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapController;
import com.google.android.maps.MapView;
import com.google.android.maps.Overlay;
import com.google.android.maps.OverlayItem;
import com.yoloswag.models.GooglePlaces;
import com.yoloswag.models.MyCityUser;
import com.yoloswag.models.Place;
import com.yoloswag.models.PlaceList;
import com.yoloswag.models.UserPin;
import com.yoloswag.mycity.GraffitiActivity;
import com.yoloswag.mycity.R;
import com.yoloswag.services.ServiceManager;
import com.yoloswag.services.SubscriptionEnum;
import com.yoloswag.services.UserService;
import com.yoloswag.services.interfaces.YoloswagListener;
import com.yoloswag.services.interfaces.YoloswagService;

public class YoloswagMapController implements YoloswagListener, LocationListener
{
	private MapController mapController;
	private MapView mapView;
	private YoloswagMapActivity activity;
	private LocationManager locationManager;
	
	public Location currentLocation;
	public GeoPoint currentPoint;
	
	MyOverlay userOverlay;
	MyOverlay placesOverlay;
	
	List<Overlay> overlays;
	
	HashMap<MyCityUser, OverlayItem> userPinToItem;
	
	String subscrType;
	ArrayList<UserPin> userPins;
	
	public GooglePlaces googlePlaces;
	public PlaceList nearPlaces;
	
	boolean findPlaces = false;
	
	public YoloswagMapController(UserService us, final YoloswagMapActivity activity)
	{
		this.activity = activity;
		setSubscriptionType(null);
		us.registerListener(this);
		userPinToItem = new HashMap<MyCityUser, OverlayItem>();		
		userPins = new ArrayList<UserPin>();
		
        mapView = (MapView) activity.findViewById(R.id.mapView);
        mapView.setBuiltInZoomControls(true);	
        mapController = mapView.getController();
        mapController.setZoom(15);
        overlays = mapView.getOverlays();
        
        activity.btnUpdate.setOnClickListener(new View.OnClickListener()
        {   
            @Override
            public void onClick(View v) 
            {
            	Location l = new Location("");
            	l.setLatitude(32.875947);
            	l.setLongitude(-117.228908);
               	setCurrentLocation(l);
               	animateToCurrentLocation();
            }
        });
        activity.findNear.setOnClickListener(new View.OnClickListener() 
        {
			@Override
			public void onClick(View v) {
				if(currentLocation != null)
				{
					findPlaces = true;
					new LoadPlaces().execute();
				}
				else
				{
					Location l = new Location("");
	            	l.setLatitude(32.875947);
	            	l.setLongitude(-117.228908);
	               	setCurrentLocation(l);
					findPlaces = true;
					new LoadPlaces().execute();
				}
					
			}
		});
        
        activity.addGraffiti.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) 
			{
                Intent addGraffiti = new Intent(activity, GraffitiActivity.class);
                activity.startActivity(addGraffiti);
			}
		});
        
        activity.shoutBtn.setOnClickListener( new View.OnClickListener() 
        {
        	@Override
        	public void onClick(View v)
        	{
        		ServiceManager.getChatService().shout(ServiceManager.getUserService().getBuddies());
        	}
        });
        
        getLastLocation();
	}
	
	/**
	 * Subscribed to a UserService
	 * 
	 * Object received will be an ArrayList of MyCityUsers with locations
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void update(Object data, Class<? extends YoloswagService> service) {
		Log.d("MapController", "Input class was: " + service.toString());
		getLastLocation();
		
		if(service == UserService.class)
		{
			@SuppressWarnings("unchecked")
			ArrayList<MyCityUser> currentUsers = (ArrayList<MyCityUser>) data;
			
			//Construct the overlay from the users given
	        Drawable marker = activity.getResources().getDrawable(R.drawable.buddy);
	        userOverlay = new MyOverlay(marker, mapView, true);
			for(MyCityUser mcu : currentUsers)
			{	
		 	    if(mcu.getLocation() != null)
		 	    {
		 	    	OverlayItem pin;
		 	    	if((pin = userPinToItem.get(mcu)) == null)
		 	    	{
				 	    Log.d("MapController", "Adding pin for user: Name: " + mcu.getRealName() + 
				 	    		" Email: " + mcu.getJid() + "Location: " + mcu.getLocation());
			    		int currLatitude = (int) (mcu.getLocation().getLatitude()*1E6);
					    int currLongitude = (int) (mcu.getLocation().getLongitude()*1E6);
					    GeoPoint point = new GeoPoint(currLatitude,currLongitude);
					    OverlayItem overlayItem = new OverlayItem(point, mcu.getRealName(), mcu.getJid());
					    userOverlay.addOverlay(overlayItem);
		 	    	} 
		 	    }
			}
			overlays.clear();
			if(userOverlay.size() > 0)
			{
				Log.d("MapController", "Adding UserOverlay");
				overlays.add(userOverlay);
			}
			if(findPlaces)
			{
				overlays.add(placesOverlay);
			}
			Runnable inval = new Runnable(){
				public void run()
				{
					mapView.invalidate();
				}
			};
			activity.runOnUiThread(inval);
//			mapView.postInvalidate();
		}
	}
	
//	public void addPin(String user, UserPin pin)
//	{
//		pins.put(user, pin);
//	}
//
//	public Collection<UserPin> getPins()
//	{
//		return pins.values();
//	}
	
    public String getBestProvider(){
        locationManager = (LocationManager) activity.getSystemService(Context.LOCATION_SERVICE);
        Criteria criteria = new Criteria();
        criteria.setPowerRequirement(Criteria.NO_REQUIREMENT);
        criteria.setAccuracy(Criteria.NO_REQUIREMENT);
        String bestProvider = locationManager.getBestProvider(criteria, true);
        return bestProvider;
    }
	
	public void getLastLocation(){
	    String provider = getBestProvider();
	    currentLocation = locationManager.getLastKnownLocation(provider);
	    if(currentLocation != null){
	        setCurrentLocation(currentLocation);
	        MyCityUser me = ServiceManager.getUserService().getMe();
	        me.setLocation(currentLocation);
	    }
	    else
	    {
//	        Toast.makeText(this.activity, "Location not yet acquired", Toast.LENGTH_LONG).show();
	    }
	}
	
	public void setCurrentLocation(Location location){
	    int currLatitude = (int) (location.getLatitude()*1E6);
	    int currLongitude = (int) (location.getLongitude()*1E6);
	    currentPoint = new GeoPoint(currLatitude,currLongitude);
	    currentLocation = new Location("");
	    currentLocation.setLatitude(currentPoint.getLatitudeE6() / 1e6);
	    currentLocation.setLongitude(currentPoint.getLongitudeE6() / 1e6);
	    Log.d("MapController", "Location is now " + currentLocation);
	}
	
	protected void onResume() {
		if(locationManager != null)
			locationManager.requestLocationUpdates(getBestProvider(), 1000, 1, this);
	}
	
	protected void onPause() {
		if(locationManager != null)
			locationManager.removeUpdates(this);
	}
	
	@Override
	public Object getSubscriptionType() {
		return subscrType;
	}


	@Override
	public void setSubscriptionType(SubscriptionEnum type) {
		subscrType = SubscriptionEnum.GPS.toString();
	}

	@Override
	public void onLocationChanged(Location location) {
	    setCurrentLocation(location);
	    if(currentPoint!=null)
	    {
	    	mapController.animateTo(currentPoint);
	    }
	    mapView.postInvalidate();
	}
	
	public void animateToCurrentLocation()
	{
	    if(currentPoint!=null)
	    {
	    	mapController.animateTo(currentPoint);
	    }
	}
	
	void drawPOIs(PlaceList nearPlaces) {
		Drawable marker = activity.getResources().getDrawable(R.drawable.restaurant);
		MyOverlay pOIs = new MyOverlay(marker, mapView, false);
		List overlays = mapView.getOverlays();
		// loop through each place
		for (Place p : nearPlaces.results) {
			int lat = (int) (p.geometry.location.lat * 1e6);
			int lng = (int) (p.geometry.location.lng * 1e6);
			OverlayItem overlayItem = new OverlayItem(new GeoPoint(lat, lng),
					p.name, p.vicinity);
			pOIs.addOverlay(overlayItem);
			Log.d("POI", "Place: " + p.name+" "+lat+ " "+lng);
		}
		
		placesOverlay = pOIs;
	}
		
	@Override
	public void onProviderDisabled(String provider) {
		
	}

	@Override
	public void onProviderEnabled(String provider) {
		
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		
	}
	
	/**
	 * Background Async Task to Load Google places
	 * */
	class LoadPlaces extends AsyncTask<String, Void, String> {

		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}

		/**
		 * getting Places JSON
		 * */
		protected String doInBackground(String... args) {
			// creating Places class object
			googlePlaces = new GooglePlaces();

			try {
			     // Separate your place types by PIPE symbol "|"
				// If you want all types places make it as null
				// Check list of types supported by google
				//
				String types = "cafe|restaurant"; // Listing places only cafes,
														// restaurants

				// Radius in meters - increase this value if you don't find any
				// places
				double radius = 2000; // 2000 meters

				if (currentLocation != null) {
					// get nearest places
						nearPlaces = googlePlaces.search(
								currentLocation.getLatitude(),
								currentLocation.getLongitude(), radius, types);
					}

				} catch (Exception e) {
					e.printStackTrace();
				}
				return null;
			}

			/**
			 * After completing background task show the data in UI Always use
			 * runOnUiThread(new Runnable()) to update UI from background thread,
			 * otherwise you will get error
			 * **/
			protected void onPostExecute(String file_url) {

				// updating UI from Background Thread
				activity.runOnUiThread(new Runnable() {
					public void run() {
						/**
						 * Updating parsed Places into LISTVIEW
						 * */
						// Get json response status
						String status = nearPlaces.status;

						// Check for all possible status
						if (status.equals("OK")) {
							// Successfully got places details
							if (nearPlaces.results != null) {
								drawPOIs(nearPlaces);
							}
						} else if (status.equals("ZERO_RESULTS")) {
							// Zero results found
							Toast.makeText(
									activity,
									"Sorry no places found. Try to change the types of places",
									Toast.LENGTH_LONG).show();
						} else if (status.equals("UNKNOWN_ERROR")) {
							Toast.makeText(activity,
									"Sorry unknown error occured.",
									Toast.LENGTH_LONG).show();
						} else if (status.equals("OVER_QUERY_LIMIT")) {
							Toast.makeText(
									activity,
									"Sorry query limit to google places is reached",
									Toast.LENGTH_LONG).show();
						} else if (status.equals("REQUEST_DENIED")) {
							Toast.makeText(activity,
									"Sorry error occured. Request is denied",
									Toast.LENGTH_LONG).show();
						} else if (status.equals("INVALID_REQUEST")) {
							Toast.makeText(activity,
									"Sorry error occured. Invalid Request",
									Toast.LENGTH_LONG).show();
						} else {
							Toast.makeText(activity,
									"Sorry error occured.", Toast.LENGTH_LONG)
									.show();
						}
					}
				});

			}
	}

}
