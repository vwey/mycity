package com.yoloswag.util;

import android.annotation.TargetApi;
import org.jivesoftware.smack.util.StringUtils;

import com.yoloswag.chat.ChatTypeEnum;
import com.yoloswag.chat.XMPPChatActivity;
import com.yoloswag.map.YoloswagMapActivity;
import com.yoloswag.models.Message;
import com.yoloswag.mycity.R;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.util.Log;

import com.yoloswag.chat.ChatTypeEnum;
import com.yoloswag.chat.XMPPChatActivity;
import com.yoloswag.models.Message;
import com.yoloswag.mycity.R;

@TargetApi(Build.VERSION_CODES.JELLY_BEAN)
/**
 * Class NotificationsManager{} Contract
 * pre-conditions: chat activity is running
 * post-conditions: user is notified of new chats and able to use notifications
 * 					to switch activities to that chat
 * invariants: main activity is not changed
 */
public class NotificationsManager {

	static Context context;
	protected static boolean hasBeenInitialized = false;
	protected static boolean contextInitialized = false;
	static int notifyID;
	
	/**
	 * init(Context context) Contract
	 * pre-conditions: context exists
	 * post-conditions: set context to current context
	 * invariants: notifyID set to 1, contextInitialized set to true
	 */
	public void init(Context context)
	{
		NotificationsManager.context = context;
		contextInitialized 	= true;
		notifyID = 1;
	}
	
	/**
	 * Context getContext() Contract
	 * pre-conditions: none
	 * post-conditions: return current context or throw error
	 * invariants: nothing changed
	 */
	public static Context getContext() 
	{
		if(contextInitialized)
			return context;
		else
			throw new IllegalStateException();
	}
	
	/**
	 * singleChatNotification(...) Contract
	 * pre-conditions: context must exist
	 * post-conditions: user must be notified of new chat window with a user
	 * 					and the conversation type
	 * invariants: main activity is not changed
	 */
	@SuppressLint("NewApi")
	public static void singleChatNotification(Context context, String jid, Message message)
	{

		if(contextInitialized)
		{
			Intent chatPage = new Intent(context, XMPPChatActivity.class);
			chatPage.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			chatPage.putExtra("jid", jid);
			chatPage.putExtra("convoType", ChatTypeEnum.SINGLE.toString());
			
			PendingIntent resultPendingIntent = PendingIntent.getActivity(
			        context,
			        notifyID,
			        chatPage,
			        PendingIntent.FLAG_UPDATE_CURRENT);
			
			Notification noti = new Notification.Builder(context)
	        .setContentTitle("New message from " + jid)
	        .setContentText(message.getBody())
	        .setSmallIcon(R.drawable.logo)
	        .setContentIntent(resultPendingIntent)
	        .setAutoCancel(true)
	        .build();
			
			NotificationManager mNotificationManager =
				    (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
				mNotificationManager.notify(notifyID++, noti);
				Log.d("NotificationsManager", "singleChatNotification sent");
		}
	}
	
	/**
	 * multiChatNotification(...) Contract
	 * pre-conditions: context must exist
	 * post-conditions: user must get a notification for a a new group chat
	 * 					with the group chat name
	 * invariants: main activity is not changed
	 */
	public static void multiChatNotification(Context context, String jid)
	{
		if(contextInitialized)
		{
			Intent chatPage = new Intent(context, XMPPChatActivity.class);
			chatPage.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			chatPage.putExtra("jid", jid);
			chatPage.putExtra("convoType", ChatTypeEnum.MULTI.toString());
			
			PendingIntent resultPendingIntent = PendingIntent.getActivity(
			        context,
			        notifyID,
			        chatPage,
			        PendingIntent.FLAG_UPDATE_CURRENT);
			
			Notification noti = new Notification.Builder(context)
	        .setContentTitle("New message in chatroom hosted by " + jid)
	        .setSmallIcon(R.drawable.logo)
	        .setContentIntent(resultPendingIntent)
	        .setAutoCancel(true)
	        .build();
			
			NotificationManager mNotificationManager =
				    (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
				mNotificationManager.notify(notifyID++, noti);
			Log.d("NotificationsManager", "multiChatNotification sent");
		}
	}
	
	public static void closeBuddyNotification(Context context, String jid)
	{
		String name = jid.split("@")[0];
		if(contextInitialized)
		{
			Intent mapPage = new Intent(context, YoloswagMapActivity.class);
			
			PendingIntent resultPendingIntent = PendingIntent.getActivity(
			        context,
			        0,
			        mapPage,
			        PendingIntent.FLAG_UPDATE_CURRENT);
			
			Notification noti = new Notification.Builder(context)
	        .setContentTitle(name + " is nearby!")
	        .setSmallIcon(R.drawable.logo)
	        .setContentIntent(resultPendingIntent)
	        .setAutoCancel(true)
	        .build();
			
			NotificationManager mNotificationManager =
				    (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
				mNotificationManager.notify(notifyID++, noti);
				Log.d("NotificationsManager", "closeBuddyChatNotification sent");
		}
	}

}
