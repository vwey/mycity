package com.yoloswag.util;

import com.yoloswag.models.MyCityUser;

public class CoordParserReverse {
	
	private static String[] returnStrings = new String[2];

	public static String[] parse(MyCityUser user){
		if(user != null){
			returnStrings[0] = "<trkpt lat=\"" + user.getLocation().getLatitude() + 
					"\" lon=\"" + user.getLocation().getLongitude() + "\">" +
					"</trkpt>";
			returnStrings[1] = user.getJid();
			return returnStrings;
		}
		else
			return null;
	}
}
