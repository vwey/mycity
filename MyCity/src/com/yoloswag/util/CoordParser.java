package com.yoloswag.util;

import java.text.CharacterIterator;
import java.text.StringCharacterIterator;

import org.jivesoftware.smackx.packet.Time;

import com.yoloswag.models.MyCityUser;

import android.location.Location;

public class CoordParser {
	
	private static int begLat, endLat;
	private static int begLon, endLon;
	private static int begTime, endTime;
	
	public static Location parse(String chatContent) {
		String[] elements = new String[10];
		Location newLoc = null;
		Time newTime = new Time();
		
		if (chatContent.startsWith("<trkpt")) {
			StringCharacterIterator chatContentIter = new StringCharacterIterator(chatContent);
			char currChatContentChar = ' ';
			
			begLat = endLat = begLon = endLon = begTime = endTime = -1;

				if(chatContent.contains("lat=\"")){
					begLat = chatContent.indexOf("lat=\"");
					currChatContentChar = chatContentIter.setIndex(begLat + 5);
					while(currChatContentChar!=CharacterIterator.DONE){
						if(currChatContentChar == '"'){
							endLat = chatContentIter.getIndex();
							elements[0] = chatContent.substring(begLat + 5, endLat);
							break;
						}
						currChatContentChar = chatContentIter.next();
					}
				}
				else
					return null;
				
				if(chatContent.contains("lon=\"")){
					begLon = chatContent.indexOf("lon=\"");
					currChatContentChar = chatContentIter.setIndex(begLon + 5);
					while(currChatContentChar!=CharacterIterator.DONE){
						if(currChatContentChar == '"'){
							endLon = chatContentIter.getIndex();
							elements[1] = chatContent.substring(begLon + 5, endLon);
							break;
						}
						currChatContentChar = chatContentIter.next();
					}
				}
				else
					return null;
			
			try{
				newLoc = new Location("");
				newLoc.setLatitude(Double.parseDouble(elements[0]));
				newLoc.setLongitude(Double.parseDouble(elements[1]));
				return newLoc;
			}
			catch(NumberFormatException e){
				return null;
			}
			
//			if(chatContent.contains("<time>")){
//				begTime = chatContent.indexOf("<time>");
//				endTime = chatContent.lastIndexOf("</time>");
//				newTime.setUtc(chatContent.substring(begTime + 6, endTime));
//			}
//			else
//				return null;

		}
		return newLoc;
	}

}
