package com.yoloswag.chat;

import java.util.ArrayList;
import java.util.Date;

import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.yoloswag.models.Conversation;
import com.yoloswag.models.Message;
import com.yoloswag.models.MyCityUser;
import com.yoloswag.mycity.R;
import com.yoloswag.services.ChatService;
import com.yoloswag.services.ServiceManager;
import com.yoloswag.services.SubscriptionEnum;
import com.yoloswag.services.interfaces.YoloswagListener;
import com.yoloswag.services.interfaces.YoloswagService;

public class ChatController implements YoloswagListener {
	Conversation 		currentConv;
	XMPPChatActivity 	activity;
	ArrayList<String>	messages;
	ChatService			chatService;
	ArrayAdapter<String> adapter;
	
	//Handles to UI elements
	EditText 	recipient;
	EditText 	textMessage;
	ListView 	listView;
	Button		sendBtn;
	TextView	textFrom;
	
	public ChatController(XMPPChatActivity activity, final EditText textMessage, final ListView listView, final Button sendBtn, final TextView textFrom)
	{
		this.activity 		= activity;
		this.recipient 		= recipient;
		this.textMessage 	= textMessage;
		this.listView 		= listView;
		this.sendBtn 		= sendBtn;
		this.textFrom		= textFrom;
		this.chatService 	= ServiceManager.getChatService();
		messages 			= new ArrayList<String>();
		
		chatService.registerListener(this);
		
  		//Get the recipient of these messages
  		Intent intent 		= activity.getIntent();
  		String jid 			= intent.getStringExtra("jid");
  		String convoType 	= intent.getStringExtra("convoType");
  		
  		textFrom.setText(jid);
  		
  		final MyCityUser toUser		= ServiceManager.getUserService().findUser(jid);
  		final MyCityUser me			= ServiceManager.getUserService().getMe();
  		
    	setListAdapterAndInvalidate();
  		
  		if( convoType.equals( ChatTypeEnum.SINGLE.toString() ) )
  		{
  			currentConv = chatService.getSingleUserChat(toUser);
  			if(currentConv == null)
  			{
  				currentConv = chatService.createNewSingleChat(me, toUser);
  			}
  		}
  		else if( convoType.equals( ChatTypeEnum.MULTI.toString() ) )
  		{
  			if(toUser != null)
  				currentConv = chatService.getMultiUserChatByOwner(toUser);
  			else // You created the room if it has no toUser
  				currentConv = chatService.getMultiUserChatByOwner(me);
  			
  		}
  		
  		sendBtn.setOnClickListener(new View.OnClickListener() 
	    {
	    	public void onClick(View view) 
	    	{
		        String body = 	textMessage.getText().toString();
	    		
		        Message newMess = new Message();
	    		newMess.setTo(toUser);
	    		newMess.setFrom(me);
	    		newMess.setBody(body);
	    		newMess.setTimestamp(new Date());
		                  
		        //Get your message to show up in the log
		        if (ServiceManager.getConnection() != null) 
		        {
		        	if(currentConv != null)
		        	{
		        		if(currentConv.getType() == ChatTypeEnum.SINGLE)
		        		{
		        			messages.add(me.getJid() + ":");
		        			messages.add(body);
		        		}
		        		chatService.sendMessage(newMess, currentConv);
		        	}
		        	else
		        	{
			        	messages.add(me.getJid() + ":");
			        	messages.add(body);
		        		currentConv = chatService.createNewSingleChat(newMess);
		        		chatService.sendMessage(newMess, currentConv);
		        	}
		        }
		        notifyChangedAndInvalidate();
		   }
		});
  		update(null, null);
	}
	
	private void setListAdapterAndInvalidate() {
		adapter = new ArrayAdapter<String>(activity, R.layout.listitem, messages);
		listView.setAdapter(adapter);
		notifyChangedAndInvalidate();
	}
	
	private void notifyChangedAndInvalidate()
	{
		activity.runOnUiThread(new Runnable()
		{
			public void run()
			{
				((BaseAdapter) listView.getAdapter()).notifyDataSetChanged();
				listView.postInvalidate();
			}
		});
		
	}

	@Override
	public void update(Object data, Class<? extends YoloswagService> service) {
		Log.d("ChatController", "Rerendering conversation.");
		//Data is null, just means that a Conversation has been updated.
		messages.clear();
		if(currentConv != null)
		{
			for(Message m : currentConv.getMessages())
			{
				messages.add(m.getFrom().getJid() + ": ");
				messages.add(m.getBody());
			}
		}
		notifyChangedAndInvalidate();
	}

	@Override
	public Object getSubscriptionType() {
		throw new UnsupportedOperationException();
	}

	//Only here to satisfy inheritance
	@Override
	public void setSubscriptionType(SubscriptionEnum type) {
		throw new UnsupportedOperationException();
	}

	public Conversation getCurrentConv() {
		return currentConv;
	}

}
