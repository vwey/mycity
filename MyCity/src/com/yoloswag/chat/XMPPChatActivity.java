package com.yoloswag.chat;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.yoloswag.mycity.R;

public class XMPPChatActivity extends Activity 
{
	ChatController cc;
	TextView recipient;
	EditText textMessage;
	ListView listView;
	Button sendBtn;
	
	public void onCreate(Bundle savedInstanceState) 
	{	
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_xmppchat);
		
	    recipient = (TextView) this.findViewById(R.id.textFrom);
	    textMessage = (EditText) this.findViewById(R.id.chatET);
	    listView = (ListView) this.findViewById(R.id.listMessages);
	    sendBtn = (Button) this.findViewById(R.id.sendBtn);
	    
		 cc = new ChatController(this, textMessage, listView, sendBtn, recipient);	
	}
	
	@Override
	public void onBackPressed() 
	{
		super.onBackPressed();
		this.finish();
	}
	
	@Override
	public void onNewIntent(Intent intent) 
	{
		cc = new ChatController(this, textMessage, listView, sendBtn, recipient);
	}
}
