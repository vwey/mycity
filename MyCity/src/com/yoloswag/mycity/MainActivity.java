package com.yoloswag.mycity;

import java.util.Collection;
import java.util.List;

import org.jivesoftware.smack.Roster;
import org.jivesoftware.smack.RosterEntry;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.packet.Presence;
import org.jivesoftware.smack.util.StringUtils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.yoloswag.map.YoloswagMapActivity;
import com.yoloswag.models.MyCityUser;
import com.yoloswag.services.ChatPacketService;
import com.yoloswag.services.ChatService;
import com.yoloswag.services.MyCoordService;
import com.yoloswag.services.ServiceManager;
import com.yoloswag.services.UserService;
import com.yoloswag.util.NotificationsManager;

public class MainActivity extends Activity {
        
        //UI elements
        private TextView        welcomeText;
        private EditText        userField;
        private EditText        passField;
        private Button          loginBtn;
        private ProgressBar 	loginProg;
        
        private MyCityApp		application; 
            
        static XMPPConnection		connection;
        public 	ServiceManager 		serviceManager;
        public  NotificationsManager	notificationManager;
        
        Handler 	servicesHandler;
        Runnable 	servicesTask;

        @Override
        protected void onCreate(Bundle savedInstanceState) {
                super.onCreate(savedInstanceState);
                application = (MyCityApp) getApplicationContext();
                setContentView(R.layout.activity_main);
                
                servicesHandler = new Handler(); 
                
                //Get handles for all UI elements in login page
                userField       = (EditText)    this.findViewById(R.id.main_userField);
                passField       = (EditText)    this.findViewById(R.id.main_passField);
                loginBtn        = (Button)      this.findViewById(R.id.main_loginBtn);
                loginProg       = (ProgressBar) this.findViewById(R.id.main_loginProg);
                
                final Context context = getApplicationContext(); 
                
                loginBtn.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) 
                {
                        loginProg.setVisibility(View.VISIBLE);
                        connect(userField.getText().toString(), passField.getText().toString());
                        
                        if(connection == null || !connection.isConnected() || !connection.isAuthenticated())
                        {
                                loginProg.setVisibility(View.INVISIBLE);
                                final Toast popup = Toast.makeText(context, "Error! Could not connect to Google Talk with" +
                                                " credentials provided. Please try again!", Toast.LENGTH_LONG);
                                popup.show();
                        }
                        else
                        {
                                final Toast popup = Toast.makeText(context, "Logged in as: " + connection.getUser(), Toast.LENGTH_LONG);
                                popup.show();
                                
                                //Start up all services
                                Location defaultLocation = new Location("");
                                defaultLocation.setLatitude(32.70257);
                                defaultLocation.setLongitude(-117.114258);
                                MyCityUser me = new MyCityUser(defaultLocation, connection.getUser(), connection.getUser());
                                serviceManager = application.getServiceManager();
                                serviceManager.init(new ChatPacketService(connection), 
                                					new UserService(connection, me), 
                                					new MyCoordService( StringUtils.parseBareAddress( connection.getUser() ) , 
              												getAllProviders(),
              												getLocationManager(),
              												connection),
              										connection,
              										new ChatService(me, connection));
                                startServiceManager();

                                notificationManager = application.getNotificationManager();
                                notificationManager.init(context);
                                
                                //Transition to the next activity here 
                                Intent mapPage 		= new Intent(getApplicationContext(), YoloswagMapActivity.class);
                                startActivity(mapPage);
                        }

                }
            });
        }
        
        @Override
        public boolean onCreateOptionsMenu(Menu menu) {
                // Inflate the menu; this adds items to the action bar if it is present.
                getMenuInflater().inflate(R.menu.activity_main, menu);
                return true;
        }
        
        /**
         * Attempts to connect to the servers and gets a list of friends.
         * @param username
         * @param password
         */
         public void connect(final String username, final String password) {
            //Connection process is on a new thread because Android throws a fit if it isn't on a new thread.
            Thread t = new Thread(new Runnable() {
              @Override
              public void run() {
                Log.d("MESSAGE", "Step 1");
                connection = MySmack.connectToGtalk(username, password);
                //setConnection(connection);
                if(connection == null || !connection.isConnected() || !connection.isAuthenticated())
                {
                        return;
                }
                Log.d("MESSAGE", "Step 2");
                Log.i("MainActivity",  "Logged in as" + connection.getUser());

                // Set the status to "yoloswag" to enable receiving GPS coords from other users
                Log.d("MESSAGE", "Step 3");

                Roster roster = connection.getRoster();
                Collection<RosterEntry> entries = roster.getEntries();
                for (RosterEntry entry : entries) {

                  Log.d("MainActivity",  "--------------------------------------");
                  Log.d("MainActivity", "RosterEntry " + entry);
                  Log.d("MainActivity", "User: " + entry.getUser());
                  Log.d("MainActivity", "Name: " + entry.getName());
                  Log.d("MainActivity", "Status: " + entry.getStatus());
                  Log.d("MainActivity", "Type: " + entry.getType());
                  Presence entryPresence = roster.getPresence(entry.getUser());

                  Log.d("MainActivity", "Presence Status: "+ entryPresence.getStatus());
                  Log.d("MainActivity", "Presence Type: " + entryPresence.getType());

                  Presence.Type type = entryPresence.getType();
                  if (type == Presence.Type.available)
                    Log.d("MainActivity", "Presence AVIALABLE");
                    Log.d("MainActivity", "Presence : " + entryPresence);
                  }
              }
            });
            t.start();
            //This thread of execution HAS to finish before we continue.
            while(t.isAlive())
            {
                try
                {
                        t.join();
                } catch(InterruptedException e) {
                        Log.e("MainActivity", "Network thread was interruped before it could be completed.");
                }
            }
         }
         
         public void setConnection(XMPPConnection connection) 
         {
                 this.connection = connection;
                 
         }
         
         private void startServiceManager()
         {
        	 final int updateTime = 1000*10; //2 sec
        	 servicesTask = new Runnable()
        	 {
        		 @Override
        		 public void run()
        		 {
        			 serviceManager.updateServices();
        			 servicesHandler.postDelayed(servicesTask, updateTime);
        		 }
        	 };
        	 servicesTask.run();
         }
         
         /////TODO!!
         /*
         private void startNotificationManager()
         {
        	 
         }
         
         */
         
         
     	public String getBestProvider(){
    	    LocationManager locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
    	    Criteria criteria = new Criteria();
    	    criteria.setPowerRequirement(Criteria.NO_REQUIREMENT);
    	    criteria.setAccuracy(Criteria.NO_REQUIREMENT);
    	    String bestProvider = locationManager.getBestProvider(criteria, true);
    	    return bestProvider;
    	}
     	
     	public List<String> getAllProviders()
     	{
     		LocationManager locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
     		return locationManager.getAllProviders();
     	}
     	
     	public LocationManager getLocationManager()
     	{
     		return (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
     	}
}