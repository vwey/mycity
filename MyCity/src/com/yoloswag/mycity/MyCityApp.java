package com.yoloswag.mycity;

import android.app.Application;

import com.yoloswag.services.ServiceManager;
import com.yoloswag.util.NotificationsManager;

public class MyCityApp extends Application {
	private ServiceManager serviceManager;
	private NotificationsManager notificationManager;

	public MyCityApp()
	{
		serviceManager = new ServiceManager();
		notificationManager = new NotificationsManager();
	}
	
	public ServiceManager getServiceManager() {
		return serviceManager;
	}

	public void setServiceManager(ServiceManager serviceManager) {
		this.serviceManager = serviceManager;
	}
	
	public NotificationsManager getNotificationManager() {
		return notificationManager;
	}

	public void setNotificationManager(NotificationsManager notificationManager) {
		this.notificationManager = notificationManager;
	}
	
}
