package com.yoloswag.mycity;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.yoloswag.map.YoloswagMapActivity;
import com.yoloswag.models.UserGraffiti;

/**
 * GraffitiActivity{} Contract
 * pre-conditions: yoloswag map controller must be active
 * post-conditions: posts an image, location, and name of a pin to
 * 					google app engine. pulls the image and name,
 * 					and puts it at the location on the map
 * invariants: none
 */

public class GraffitiActivity extends Activity {
	
	public static final String PRODUCT_URI = "http://kittycathumandog1.appspot.com/product";
	public static final String ITEM_URI = "http://kittycathumandog1.appspot.com/item";
	
	Button tagGraffiti;
	Button uploadImage;
	Button defaultImage;
	
	ImageView image;
	
	private EditText graffiti_name; 
	
	private String TAG = "TaggingGraffiti";
	
	private int SELECT_IMAGE;


	/**
	 * 
	 * onCreate() Contract
	 * pre-conditions: GraffitiActivity must be running
	 * post-conditions: Updates views for tagGraffiti button, uploadImage
	 * 					button, defaultImage button, image, and graffiti_name.
	 * 					Updates onClickListeners for tagGraffiti button,
	 * 					uploadImage button, and defaultImage button.
	 * invariants: Focus of the layout doesn't change
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_graffiti);		
		graffiti_name = (EditText) findViewById(R.id.graffiti_name);
		
		tagGraffiti = (Button) findViewById(R.id.addGraffiti);
		uploadImage = (Button) findViewById(R.id.uploadImage);
		defaultImage = (Button) findViewById(R.id.defaultImage);
		
		image = (ImageView) findViewById(R.id.imageView1);
		
		defaultImage.setOnClickListener(new View.OnClickListener()
		{
			/**
			 * onClick() Contract
			 * pre-condition: defaultImage button is clicked
			 * post-condition: Uploads the view with an image thumbnail of the
			 * 					default image.
			 * invariants: Focus of the layout doesn't change
			 */
			@Override
			public void onClick(View v) {
				getDefaultImage();
			}
			
		});
		
		uploadImage.setOnClickListener(new View.OnClickListener()
		{
			/**
			 * onClick() Contract
			 * pre-condition: uploadImage button is clicked
			 * post-condition: Uploads the view with an image thumbnail of the
			 * 					uploaded image.
			 * invariants: Focus of the layout doesn't change
			 */
			@Override
			public void onClick(View v) {
				getUploadImage();
				
			}
		});
		

		tagGraffiti.setOnClickListener(new View.OnClickListener()
		{
			/**
			 * onClick() Contract
			 * pre-condition: tagGraffiti button is clicked
			 * post-condition: Posts the image to the google app engine and returns
			 * 					to YoloswagMapActibvity.
			 * invariants: none
			 */
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				postdata();
				Intent myIntent = new Intent(GraffitiActivity.this, YoloswagMapActivity.class);
				startActivity(myIntent);
			}
			
		});
	}
	
	/**
	 * getDefaultImage() Contract
	 * pre-condition: image must be set to a view
	 * post-condition: image is set to drawable.defaultimage
	 * invariants: none
	 */
	private void getDefaultImage()
	{
		image.setImageResource(R.drawable.defaultimage);
	}

	/**
	 * getUploadImage() Contract
	 * pre-condition: none
	 * post-condition: Starts a new activity with Intent.ACTION_PICK. allows user to
	 * 					select image to upload from phone gallery.
	 * invariants: none
	 */
	private void getUploadImage()
	{
		startActivityForResult(new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.INTERNAL_CONTENT_URI), SELECT_IMAGE);
	}
		
	
		/**
		 * inherits contract
		 */
		@Override
		public boolean onCreateOptionsMenu(Menu menu) {
			// Inflate the menu; this adds items to the action bar if it is present.
//			getMenuInflater().inflate(R.menu.activity_main, menu);
			return true;
		}
		
		/**
		 * onActivityResult(int requestCode, int resultCode, Inten data) Contract
		 * pre-condition: SELECT_IMAGE has been set and and startActivityForResult has
		 * 					been called.
		 * post-condition: Gets the URI of the image and sets image to be that URI
		 * invariants: none
		 */
		@Override
		public void onActivityResult(int requestCode, int resultCode, Intent data) {
		  super.onActivityResult(requestCode, resultCode, data);
		  if (requestCode == SELECT_IMAGE)
		    if (resultCode == Activity.RESULT_OK) {
		      Uri selectedImage = data.getData();
		      image.setImageURI(selectedImage);
		    } 
		}

		/**
		 * postdata() Contract
		 * pre-condition: none
		 * post-condition: Posts image, name, and location to the google app engine
		 * invariants: none
		 */
		private void postdata() {
			final ProgressDialog dialog = ProgressDialog.show(this,
					"Posting Data...", "Please wait...", false);
			Thread t = new Thread() {

				public void run() {
					HttpClient client = new DefaultHttpClient();
					HttpPost post = new HttpPost(GraffitiActivity.PRODUCT_URI);

				    try {
				      List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
				      nameValuePairs.add(new BasicNameValuePair("coords",
				    		  graffiti_name.getText().toString()));
				      nameValuePairs.add(new BasicNameValuePair("action",
					          "put"));
				      post.setEntity(new UrlEncodedFormEntity(nameValuePairs));
				      
				      HttpResponse response = client.execute(post);
				      BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
				      String line = "";
				      while ((line = rd.readLine()) != null) {
				        Log.d(TAG, line);
				      }

				    } catch (IOException e) {
				    	Log.d(TAG, "IOException while trying to conect to GAE");
				    }
					dialog.dismiss();
				}
			};

			t.start();
			dialog.show();
		}
		
		/**
		 * GetImage extends AsyncTask, Void, List<String>>{}
		 * pre-condition: Image, name, and location have been posted to google
		 * 					app engine
		 * post-condition: Image, name, and location are pulled from the google
		 * 					app engine. Image is posted on map with name based on
		 * 					location.
		 * invariants: none.
		 */
//		private class GetImage extends AsyncTask<String, Void, List<String>> {
//
//			 @Override
//		     protected List<String> doInBackground(String... url) {
//			//Makes a GET HTTP request like 
//			// AddItemActivity
//				 
//		    	 HttpClient client = new DefaultHttpClient();
//					HttpGet request = new HttpGet(url[0]);
//					List<String> list = new ArrayList<String>();
//					try {
//						HttpResponse response = client.execute(request);
//						HttpEntity entity = response.getEntity();
//						String data = EntityUtils.toString(entity);
//						Log.d(TAG, data);
//						JSONObject myjson;
//						
//						try {
//							myjson = new JSONObject(data);
//							JSONArray array = myjson.getJSONArray("data");
//							for (int i = 0; i < array.length(); i++) {
//								JSONObject obj = array.getJSONObject(i);
//								list.add(obj.get("coords").toString());
//							}
//							
//						} catch (JSONException e) {
//
//					    	Log.d(TAG, "Error in parsing JSON");
//						}
//						
//					} catch (ClientProtocolException e) {
//
//				    	Log.d(TAG, "ClientProtocolException while trying to connect to GAE");
//					} catch (IOException e) {
//
//						Log.d(TAG, "IOException while trying to connect to GAE");
//					}
//		        return list;
//		     }
//			 
			/**
			 * onPostExecute(List<String> list) Contract
			 * pre-condition: GetImage has been called and image, name, and location
			 * 					are pulled from google app engine
			 * post-condition: Displays image with its name on the map at the specified
			 * 					location
			 * invariants: none.
			 */
//		     protected void onPostExecute(List<String> list) 
//		     {
//		    	String location = "";
//		    	String[] latLon;
//		    	ArrayList<UserGraffiti> graffitiCollection = new ArrayList<UserGraffiti>();
//		    	
//		 		for(int i = 0; i<list.size(); i++)
//		 		{
//		 			location = list.get(i);
//	 				latLon = location.split(",");
//		 			UserGraffiti graffiti = new UserGraffiti();
//	 				graffiti.updateLocation(Double.valueOf(latLon[0]), Double.valueOf(latLon[1]));
//	 				graffitiCollection.add(graffiti);
//		 		}
//			}
//		}
	}
