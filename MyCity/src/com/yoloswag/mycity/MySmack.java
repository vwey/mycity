package com.yoloswag.mycity;

import java.text.CharacterIterator;
import java.text.StringCharacterIterator;

import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smack.packet.Presence;

import android.location.Location;
import android.util.Log;

import com.yoloswag.models.MyCityUser;

/****
 * 
 * @author cs110w
 * 
 *         This class defines a layer between aSmack library and the application, such that:
 * 
 *         1. it has a simpler interface to work with 
 *         2. the library can be more readable 
 *         3. it will make the test easier
 * 
 *         In this class we will require you to define your own interface on top of aSmack
 * 
 */

public class MySmack {

	/* Section-1 */
	// A sample of setting up a logged-in connection to Gtalk Server
	// 		Usage: connectToGtalk("username", "password");
	public static XMPPConnection connectToGtalk(String username, String password) {
		ConnectionConfiguration connConfig = new ConnectionConfiguration(
				"talk.google.com", 5222, "gmail.com");
                MainActivity.connection = new XMPPConnection(connConfig);
		try {
			MainActivity.connection.connect();
			Log.i("XMPP - Connection", "Connected to " + MainActivity.connection.getHost());
		} catch (XMPPException e) {
			Log.e("XMPP - Connection", "Failed to connect to  Gtalk server.");
		}
		try {
			MainActivity.connection.login(username, password);
			Log.i("XMPP - Logging in", "Logged in as " + MainActivity.connection.getUser());
		} catch (XMPPException e) {
			Log.e("XMPP - Logging in", "Failed to log in as " + username + ".");
		}
		return MainActivity.connection;
	}

	
	/* Section-2 */
	// An exercise of setting up a logged-in connection to Gtalk Server,
	// which sets the present status upon logging in.
	// 		Usage: connectToGtalk("username", "password", "available");
	// Note that the last parameter is the presence type, which can be:
	// 		either "available", or "unavailable"
	// Also note that the last parameter is of type String.

	public static XMPPConnection connectToGtalk(String username, String password, String presence) {
		ConnectionConfiguration connConfig = new ConnectionConfiguration(
				"talk.google.com", 5222, "gmail.com");
                MainActivity.connection = new XMPPConnection(connConfig);
		try {
			MainActivity.connection.connect();
			Log.i("XMPP - Connection", "Connected to " + MainActivity.connection.getHost());
		} catch (XMPPException e) {
			Log.e("XMPP - Connection", "Failed to connect to  Gtalk server.");
		}
		try {
			MainActivity.connection.login(username, password);
			Presence p;
			if(presence.equals("available"))
				p = new Presence(Presence.Type.available);
			else
				p = new Presence(Presence.Type.unavailable);
			MainActivity.connection.sendPacket(p);
			Log.i("XMPP - Logging in", "Logged in as " + MainActivity.connection.getUser());
		} catch (XMPPException e) {
			Log.e("XMPP - Logging in", "Failed to log in as " + username + ".");
		}
		return MainActivity.connection;
	}

	
	/* Section-3 */
	// An sample enhanced functionality
	// 		Usage:
	// Upon receiving a chat message, check to see if it is in the format
	// 		[APPOINTMENT]sometime@somelocation (e.g. "[APPOINTMENT]12:30pm@UCSD Library")
	// if it is not, treat it as a normal chat message
	// but if it is, process it and, before posting it, reformat it into
	// 		You have an appointment on sometime at some location.
	// 		e.g. "You have an appointment on 12:30pm at UCSD Library."
	public static String processTrackPoint(Packet packet) {
		Message message = (Message) packet;
		String chatContent = message.getBody();
		String[] elements = new String[10];
		
		if (chatContent.startsWith("<trkpt")) {
			StringCharacterIterator chatContentIter = new StringCharacterIterator(chatContent);
			char currChatContentChar = chatContentIter.next();
			
			int begLat = -1;
			int endLat = -1;
			
			int begLon = -1;
			int endLon = -1;
					
			while(currChatContentChar!=CharacterIterator.DONE){
				if(currChatContentChar == '"'){
					if(begLat == -1)
						begLat = chatContentIter.getIndex();
					else if(endLat == -1){
						endLat = chatContentIter.getIndex();
						elements[0] = chatContent.substring(begLat + 1, endLat);
					}
					else if(begLon == -1)
						begLon = chatContentIter.getIndex();
					else if(endLon == -1){
						endLon = chatContentIter.getIndex();
						elements[1] = chatContent.substring(begLon + 1, endLon);
					}	
				}
				currChatContentChar = chatContentIter.next();
			}
			
			Location newLoc = new Location("");
			newLoc.setLatitude(Double.parseDouble(elements[0]));
			newLoc.setLongitude(Double.parseDouble(elements[1]));
			
			//TODO: huuuh???? why are we using grady's shit??
			MyCityUser user = new MyCityUser(newLoc, "grady@gmail.com", "Grady Kestler");

			return "Name: " + user.getJid() 
					+ "\nLatitude: " + user.getLocation().getLatitude() 
					+ "\nLongitude: " + user.getLocation().getLongitude();
		}
		else
			return chatContent;
	}
	
	
	/* Section-4 */
	// An exercise: consider the example in Section-3, what if we want to
	// be able to process the string according to the regular expression
	// specified by the developers? For example, your developers may want
	// to have the flexibility to pass this method a regular expression, 
	// which can be: 
	//		regex1, regex2, ..., or regexN
	// according to which, the transformation process can be:
	//		transform1(string), transform2(string), ..., transformN(string)
	// Define your own usage format, and write down your *pseudo-code*
	
	// 		Usage: processAppointment(Packet packet, boolean transform)
	//					if(matches expression && transform == true)
	//						transform message appropriately
	//					else return chat content normally
	
	
	/* Section-5 */
	// Answer this: assuming the chat content can be an XML format string
	// for special purposes, can you process it in the way you did in the
	// above two sections, i.e., try to define a regular expression, match
	// and then process the string?
	// If you answer "yes", tell us how would you do that;
	// if you answer "no", tell us why, and what would you do alternatively.
	
	/**
	 * Yes because we included a conditional parameter that gave the user the option to 
	 * process the string differently. If that parameter were false, it would output the
	 * message exactly how it was input.
	 */

}

