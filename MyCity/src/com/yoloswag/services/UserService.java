package com.yoloswag.services;

import java.util.ArrayList;
import java.util.Collection;

import org.jivesoftware.smack.Roster;
import org.jivesoftware.smack.RosterEntry;
import org.jivesoftware.smack.XMPPConnection;

import android.util.Log;

import com.yoloswag.models.MyCityUser;
import com.yoloswag.services.interfaces.YoloswagListener;
import com.yoloswag.services.interfaces.YoloswagService;
import com.yoloswag.util.NotificationsManager;


/**
 * Responsibilities:
 * - Manages status and location for all other users 
 * @author
 *
 */
public class UserService implements YoloswagService,
									  YoloswagListener
{
	private ArrayList<MyCityUser> myBuddies;
	private ArrayList<MyCityUser> closeBuddies;
	private ArrayList<YoloswagListener> listeners;
	private SubscriptionEnum subscription;
	private XMPPConnection connection;
	private MyCityUser me;
	
	static float PROXIMITY = 2000; //meters. if buddies come within this distance, send notification.
	 
	/**
	 * UserService(XMPPConnection connection) contract:
	 * 
	 * pre-conditions: connection is not null, me is not null
	 * post-conditions: myBuddies initialized as a blank array
	 * 					listeners initialized as a blank array
	 * 					Set connection to active connection
	 * invariants: listeners, myBuddies, subscription, connection unchanged
	 */
	public UserService(XMPPConnection connection, MyCityUser me)
	{
		setSubscriptionType(null);
		myBuddies = new ArrayList<MyCityUser>();
		closeBuddies = new ArrayList<MyCityUser>();
		listeners = new ArrayList<YoloswagListener>();
		this.connection = connection;
		this.me = me;
	}
	
	/**
	 * updateBuddies() contract
	 * pre-conditions: Roster containing buddies exists
	 * post-conditions: myBuddies is updated containing only active buddies
	 * 					contained on the Roster
	 * invariants: listeners, subscription, connection unchanged
	 */
	private void updateBuddies()
	{
		Roster roster = connection.getRoster();
		
		Collection<RosterEntry> rosterCollection = roster.getEntries();
		for(RosterEntry r: rosterCollection)
		{
			if(findUser(r.getUser()) == null)
			{
				MyCityUser user = new MyCityUser(null, r.getUser(), r.getUser());
				myBuddies.add(user);
			}
		}
		for(MyCityUser user: myBuddies)
		{
			String tempJid = user.getJid();
			boolean isFound = false;
			
			for(RosterEntry r: rosterCollection)
			{
				if(r.getUser().equals(tempJid))
				{
					isFound = true;
					if(me.getLocation() != null && user.getLocation() != null)
					{
						if(me.getLocation().distanceTo(user.getLocation()) <= PROXIMITY)
						{
							if(!closeBuddies.contains(user))
							{
								closeBuddies.add(user);
								NotificationsManager.closeBuddyNotification(NotificationsManager.getContext(), tempJid);
							}
						}
						else
							closeBuddies.remove(user);
					}
				}
					
			}
			if(isFound == false) 
			{
				myBuddies.remove(user);
				closeBuddies.remove(user);
			}
		}
	}
	 
	/**
	 * MyCityUser findUser(String jid) contract
	 * pre-conditions: jid is not null
	 * post-conditions: if the jid exists in myBuddies, return the MyCityUser
	 * 					containing that jid.  Else return null.
	 * invariants: listeners, myBuddies, subscription, connection unchanged
	 */
	public MyCityUser findUser(String jid)
	{
		for(MyCityUser buddy: myBuddies)
		{
			if(jid.equals(buddy.getJid())) return buddy;
		}
		return null;
	}
	
	/**
	 * ArrayList<MyCityUser> getBuddies() contract
	 * pre-conditions: myBuddies is initialized.
	 * post-conditions: myBuddies is returned
	 * invariants: listeners, myBuddies, subscription, connection unchanged
	 */
	public ArrayList<MyCityUser> getBuddies()
	{
		return myBuddies;
	}

	/**
	 * contract inherited
	 */
	public void registerListener(YoloswagListener l) 
	{
		listeners.add(l);
	}

	/**
	 * contract inherited
	 */
	public void removeListener(YoloswagListener l) 
	{
		listeners.remove(l);
	}

	/**
	 * updateListeners() contract
	 * pre-conditions: listeners exists
	 * post-conditions: each listener is updated with myBuddies
	 * invariants: listeners, myBuddies, subscription, connection unchanged
	 */
	public void updateListeners() 
	{
		for(YoloswagListener yolo: listeners)
		{
			yolo.update(myBuddies, UserService.class);
		}
	}

	/**
	 * contract inherited from methods called
	 */
	public void update() 
	{
		updateBuddies();
		updateListeners();
	}

	/**
	 * void update(...) contract
	 * pre-conditions: myBuddies exists, data is not null, service is not null
	 * post-conditions: buddy is updated with trackpoint data
	 * invariants: listeners, subscription, connection unchanged
	 */
	public void update(Object data, Class<? extends YoloswagService> service) 
	{	
		MyCityUser user = (MyCityUser) data;
		if(user.getLocation() != null)
		{
			MyCityUser temp = findUser(user.getJid());
			Log.d("RosterService","Attemping to update user: " + temp.getJid() + " with data " + user.getLocation());
			if(temp != null) temp.setLocation(user.getLocation());
		}
		update();
	}
 	
	/**
	 * Object setSubscriptionType() contract
	 * pre-conditions: subscription exists
	 * post-conditions: subscription returned
	 * invariants: listeners, myBuddies, subscription, connection unchanged
	 */
	@Override
	public Object getSubscriptionType() 
	{
		return subscription;
	}

	/**
	 * void setSubscriptionType(SybscriptionEnum type) contract
	 * pre-conditions: type is not null
	 * post-conditions: subscription is set to new subscription
	 * invariants: listeners, myBuddies, connection unchanged
	 */
	@Override
	public void setSubscriptionType(SubscriptionEnum type) 
	{
		subscription = SubscriptionEnum.GPS;
	}

	public MyCityUser getMe() {
		return me;
	}
}