package com.yoloswag.services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.UUID;

import org.jivesoftware.smack.Connection;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.util.StringUtils;
import org.jivesoftware.smackx.muc.InvitationListener;
import org.jivesoftware.smackx.muc.MultiUserChat;

import android.util.Log;

import com.yoloswag.chat.ChatTypeEnum;
import com.yoloswag.models.Conversation;
import com.yoloswag.models.Message;
import com.yoloswag.models.MyCityUser;
import com.yoloswag.services.interfaces.YoloswagListener;
import com.yoloswag.services.interfaces.YoloswagService;
import com.yoloswag.util.NotificationsManager;

public class ChatService implements YoloswagListener, YoloswagService {
	XMPPConnection connection;
	ArrayList<Conversation> conversations;
	MyCityUser currentAppUser;
	
	Collection<YoloswagListener> listeners;
	
	SubscriptionEnum subscrType;
	
	public ChatService(MyCityUser currUser, XMPPConnection conn)
	{
		conversations = new ArrayList<Conversation>();
		listeners = new ArrayList<YoloswagListener>();
		setSubscriptionType(null);
		currentAppUser = currUser;
		connection = conn;
		createShoutListener();
	}
	
	/**
	 * Specifications
	 * 
	 * Payload:
	 * Message: a chat message from a single member
	 */
	@Override
	public void update(Object data, Class<? extends YoloswagService> service) {
		Message message = (Message) data;
		MyCityUser user = message.getFrom();
		
		Log.d("ChatService", "Got a message from: " + message.getFrom().getJid() + " | Message: " + message.getBody());
		
		
		for(Conversation c : conversations)
		{
			if(c.getParticipants().contains(user))
			{
				Log.d("ChatService", "Updated conversation: " + c);
				c.addMessage(message);
				update();
				NotificationsManager.singleChatNotification(NotificationsManager.getContext(), message.getFrom().getJid(), message);
				return;
			}
		}
		Conversation c = createNewSingleChat(message);
		Log.d("ChatService", "Created a new conversation and added it to conversations");
		
		
		update();
		
		NotificationsManager.singleChatNotification(NotificationsManager.getContext(), message.getFrom().getJid(), message);
	}

	@Override
	public Object getSubscriptionType() {
		return subscrType;
	}

	@Override
	/**
	 * This object can only have a subscription type of "CHAT", so this is irrelevant.
	 */
	public void setSubscriptionType(SubscriptionEnum type) 
	{
		subscrType = SubscriptionEnum.CHAT;
	}
	
	public void sendMessage(Message m, Conversation currentConv)
	{
		if(currentConv.getType() == ChatTypeEnum.SINGLE)
		{
			org.jivesoftware.smack.packet.Message smackMessage =
				new org.jivesoftware.smack.packet.Message(m.getTo().getJid());
			smackMessage.addBody("", m.getBody());
	    	currentConv.addMessage(m);
			connection.sendPacket(smackMessage);
		}
		if(currentConv.getType() == ChatTypeEnum.MULTI)
		{
			//currentConv.addMessage(m);
			MultiUserChat muc = currentConv.getChatroom();
			try
			{
				m.setBody(m.getFrom().getJid() + " says " +m.getBody());
				muc.sendMessage(m.getBody());
			}
			catch(XMPPException ex)
			{
				Log.d("ChatService", "Was unable to send message to chatroom \"" + muc.getRoom() +
						"\". Message: " + m.getBody());
			}
		}
	}
	
	public void shout(ArrayList<MyCityUser> closeBuddies)
	{
		String roomName = "private-chat-" + UUID.randomUUID().toString() + "@groupchat.google.com";
		MultiUserChat muc = new MultiUserChat(connection, roomName);
		try {
			//Current user creates a chat room
			String nick = ServiceManager.getUserService().getMe().getJid() + ",owner";
			muc.join(nick);
			
			//Configure it as an unmoderated room
			//RG 3/15: Gtalk rooms are not configurable
//		    Form form = muc.getConfigurationForm();
//		    Form answerForm = form.createAnswerForm();
//		    answerForm.setAnswer("muc#roomconfig_unmoderatedroom", "1");
//		    muc.sendConfigurationForm(answerForm);
		    
		    //Invite everyone close enough to the shout
			MyCityUser me = ServiceManager.getUserService().getMe();
		    for(MyCityUser user : closeBuddies)
		    {
		    	org.jivesoftware.smack.packet.Message smackMessage = new org.jivesoftware.smack.packet.Message(user.getJid());
		    	muc.invite(user.getJid(), "It's a shout!");
		    }
		    
		    //Create a new conversation from the chat room
		    Conversation chatRoomConversation = new Conversation(muc);
		    chatRoomConversation.getOwners().add(me);
		    conversations.add(chatRoomConversation);
		    
			NotificationsManager.multiChatNotification(NotificationsManager.getContext(), 
					me.getJid());
		    
		} catch (XMPPException e) 
		{
			Log.e("CHAT_CONTROLLER", "Error creating chat/configuring chat room.");
		}
	}
	
	public void createShoutListener()
	{
		MultiUserChat.addInvitationListener(connection, new InvitationListener() {
			public void invitationReceived(Connection conn, String room, String inviter, String reason, String password, org.jivesoftware.smack.packet.Message message)
			{
				MultiUserChat muc = new MultiUserChat(connection, room);
				
				try {
					muc.join(room);
					String nick = ServiceManager.getUserService().getMe().getJid() + ",user";
					muc.changeNickname(nick);
					
					Iterator<String> users = muc.getOccupants();
					String owner = null;
					
					while(users.hasNext())
					{
						String curUser = users.next();
						String nickname = StringUtils.parseResource(curUser);
						String[] split = nickname.split(",");
						if(split.length == 2)
						{
							if(split[1].equals("owner"))
							{
								owner = split[0];
								break;
							}
						}
					}
					
					NotificationsManager.multiChatNotification(NotificationsManager.getContext(), 
																owner);
					
					Conversation chatRoomConversation = new Conversation(muc);
			
					conversations.add(chatRoomConversation);
					
				} catch (XMPPException e) {
					Log.e("CHAT_CONTROLLER", "Error joining chat room from shout invitation");
				}
	        }
	    });
	}
	
	public Conversation getSingleUserChat(MyCityUser user)
	{
		Conversation returnConvo = null;
		for(Conversation c : conversations)
		{
			if(c.getParticipants().contains(user))
				returnConvo = c;
		}
		return returnConvo;
	}
	
	public Conversation getMultiUserChatByOwner(MyCityUser owner)
	{
		Conversation returnConvo = null;
		for(Conversation c : conversations)
		{
			MultiUserChat muc;
			if( (c.getType() == ChatTypeEnum.MULTI) && (( muc = c.getChatroom() ) != null))
			{
				for(MyCityUser o : c.getOwners())
				{
					if(o == null) { continue; }
					Log.d("ChatService", "o: " + o + " | owner: " + owner);
					boolean result = o.getJid().equals(owner.getJid());
					Log.d("ChatService", "Comparing strings \"" + o.getJid() + 
							"\" and \"" + owner.getJid() + "\". Result = "
							+ result);
					
					if(o.getJid().equals(owner.getJid()))
					{
						return c;
					}
				}
			}
		}		
		return returnConvo;
	}

	@Override
	public void registerListener(YoloswagListener l) {
		listeners.add(l);
	}

	@Override
	public void removeListener(YoloswagListener l) {
		listeners.remove(l);
	}

	@Override
	public void updateListeners() {
		for(YoloswagListener l : listeners)
		{
			l.update(null, ChatService.class);
		}
	}

	@Override
	public void update() {
		Log.d("ChatService", "Updating Listeners");
		updateListeners();		
	}

	public Conversation createNewSingleChat(Message newMess) {
		Conversation c = new Conversation(newMess.getFrom());
		c.getParticipants().add(newMess.getFrom());
		c.getParticipants().add(newMess.getTo());
		c.addMessage(newMess);
		conversations.add(c);
		return c;
	}
	public Conversation createNewSingleChat(MyCityUser me, MyCityUser to) {
		Conversation c = new Conversation(me);
		c.getParticipants().add(me);
		c.getParticipants().add(to);
		conversations.add(c);
		return c;
	}
}
