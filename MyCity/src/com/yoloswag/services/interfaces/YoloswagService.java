package com.yoloswag.services.interfaces;

import java.util.ArrayList;


/**
 * An interface for basic services using the observer pattern.
 * 
 * Services are parts of the program that run in the background constantly for some purpose.
 * @author Ryan
 *
 */
public interface YoloswagService {		
	 	/**
	 	 * Adds a listener to the list of listeners
	 	 * @param l
	 	 */
		public void registerListener(YoloswagListener l);
		/**
		 * Removes a given listener
		 * @param l
		 */
		public void removeListener(YoloswagListener l);
		
		/**
		 * Updates all listeners with latest data. Please SPECIFY what type of data is being
		 * sent in implementations!
		 */
		public void updateListeners();
		
		/**
		 * The main update loop for this service.
		 */
		public void update();
		
}
