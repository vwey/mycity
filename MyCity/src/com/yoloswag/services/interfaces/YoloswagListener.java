package com.yoloswag.services.interfaces;

import com.yoloswag.services.SubscriptionEnum;

/**
 * An interface for listeners taken from the Observer pattern.
 * @author Ryan
 *
 */
public interface YoloswagListener {
	/**
	 * What should be done with the listener when it is updated. Takes a data object as an
	 * argument. Please see the related Service for what the data object is.
	 */
	public void update(Object data, Class<? extends YoloswagService> service);
	
	/**
	 * Returns the subscription type (if applicable). Please document what you are using for subscriptions so we can cast appropriately!
	 * If you do not use this feature, throw an UnsupportedOperationException 
	 * @return
	 */
	public Object getSubscriptionType();
	/**
	 * Sets the subscription type (if applicable). Please document what you are using for subscriptions so we can cast appropriately
	 * If you do not use this feature, throw an UnsupportedOperationException
	 */
	public void setSubscriptionType(SubscriptionEnum type);
}
