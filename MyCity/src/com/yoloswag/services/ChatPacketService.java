package com.yoloswag.services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import org.jivesoftware.smack.PacketListener;
import org.jivesoftware.smack.RosterEntry;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.filter.MessageTypeFilter;
import org.jivesoftware.smack.filter.PacketFilter;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smack.util.StringUtils;

import android.location.Location;
import android.os.Handler;
import android.util.Log;

import com.yoloswag.models.Message;
import com.yoloswag.models.MyCityUser;
import com.yoloswag.services.interfaces.YoloswagListener;
import com.yoloswag.services.interfaces.YoloswagService;
import com.yoloswag.util.CoordParser;

/**
 * Responsibility: handle all incoming XMPP chat message packets and redirect them to the  
 * them to the appropriate controllers.
 * 
 *
 * @author Ryan
 *
 */
public class ChatPacketService implements YoloswagService {

	ArrayList<YoloswagListener> listeners;
	ArrayList<String> messageBuffer;
	ArrayList<Message> chatOutput;
	ArrayList<MyCityUser> gpsOutput;
	Collection<RosterEntry> entries;
	XMPPConnection connection;
	private Handler messageHandler = new Handler();
	String currentMessage;
	
	final String DELIMITER = "-->";
	
	/**
	 * Constructor: takes in the connection for the current user 
	 * @param connection
	 */
	public ChatPacketService(XMPPConnection connection) throws IllegalArgumentException
	{
		if(connection == null)
		{
			throw new IllegalArgumentException();
		}
		this.connection = connection;
		
		listeners 		= new ArrayList<YoloswagListener>();
		messageBuffer 	= new ArrayList<String>();
		chatOutput 		= new ArrayList<Message>();
		gpsOutput 		= new ArrayList<MyCityUser>();
		
		PacketFilter filter 	= new MessageTypeFilter( org.jivesoftware.smack.packet.Message.Type.chat);
		PacketListener listener = new PacketListener() 
		{
			@Override
			public void processPacket(Packet packet) 
			{
				org.jivesoftware.smack.packet.Message message = (org.jivesoftware.smack.packet.Message) packet;
				String body = message.getBody();
				if (body != null) {
		            String fromName = StringUtils.parseBareAddress(message.getFrom());
		            String toName = StringUtils.parseBareAddress(message.getTo());
		            currentMessage = fromName + DELIMITER + body + DELIMITER + toName;
		            Log.d("PacketService", "Message inserted into buffer: " + currentMessage);
					messageBuffer.add(currentMessage);
				}
				update();
				updateListeners();
            }
		};
		
		this.connection.addPacketListener(listener, filter);
	}
	
	@Override
	public void registerListener(YoloswagListener l) {
		listeners.add(l);
	}

	@Override
	public void removeListener(YoloswagListener l) {
		listeners.remove(l);
	}

	@Override
	/**
	 * Specifications
	 * Two listener types, identified by their getSubscriptionType enum :
	 * CHAT - deals with all chat messages
	 * GPS  - deals with all GPS messages
	 *
	 * Payload:
	 * For CHAT - Message object
	 * For GPS  - Location object speicifying the current location of the user
	 *
	 */
	public void updateListeners() {
		Log.v("PacketService", "Updating listeners. GPS: " + gpsOutput.size() + " | CHAT: " + chatOutput.size());
		Log.v("PacketService", "Current listeners are: ");

		for(YoloswagListener l : listeners)
			Log.v("PacketService", l.getClass().toString() + " listening as " + l.getSubscriptionType());
		
	
		for(YoloswagListener l : listeners)
		{
			Log.d("PacketService", "Updating " + l.getClass().toString());
			SubscriptionEnum subscriptionType = (SubscriptionEnum) l.getSubscriptionType();
			if( subscriptionType.equals (SubscriptionEnum.CHAT) )
			{
				for( Message message : chatOutput)
					l.update(message, ChatPacketService.class);
			}
			else if( subscriptionType.equals( SubscriptionEnum.GPS ) )
			{
				for( MyCityUser user : gpsOutput)
					l.update(user, ChatPacketService.class);
			}
		}
		//Clear the buffers
		chatOutput.clear();
		gpsOutput.clear();
		Log.d("PacketService", "After update. GPS: " + gpsOutput.size() + " | CHAT: " + chatOutput.size());
		Log.d("PacketService", "Finised updating listeners.");
	}

	@Override
	public void update() {
		//Read received messages		
		if(!messageBuffer.isEmpty())
		{
			//Determine the type of message by running it through the parsers
			for(String message : messageBuffer)
			{
				String[] split = message.split(DELIMITER);
				Location location;
				//We have a location. Send off to UserLocation wrapping
				//TODO: parsing needs to be fixed for name variable
				String name = "NAME";
				if( (location = CoordParser.parse(split[1])) != null )
				{
					Log.d("PacketService", "Added a trkpt location to the GPS output: " + split[1] + " from: " + split[0]);
					MyCityUser newUser = new MyCityUser(location, split[0], split[0]);
					gpsOutput.add(newUser);
				}
				else //Otherwise it's just a normal message
				{
					MyCityUser from = ServiceManager.getUserService().findUser(split[0]);
					MyCityUser to = ServiceManager.getUserService().findUser(split[2]);
					Message newMes = new Message();
					newMes.setBody(split[1]);
					newMes.setTo(to);
					newMes.setFrom(from);
					newMes.setTimestamp(new Date());
					Log.d("PacketService", "Added a message to the chat output: " + split[1] + " from: " + split[0]);
					chatOutput.add(newMes);
				}
			}
			//Clear message buffer when we're done
			messageBuffer.clear();
			updateListeners();
		}
	}
	
	/**
	 * Sends a chat message
	 * @param msg the actual content of the message
	 * @param recipient the JID of who is receiving the message
	 */
	public void sendMessage(String body, String recipient)
	{
		Log.d("PacketService", "Message sent. Message: " + body + " | Recipient: " + recipient);
		org.jivesoftware.smack.packet.Message message = new org.jivesoftware.smack.packet.Message(recipient, org.jivesoftware.smack.packet.Message.Type.chat);  
        message.setBody(body);
        connection.sendPacket(message);
	}
	
}
