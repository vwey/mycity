package com.yoloswag.services;

import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.util.StringUtils;

import com.yoloswag.models.MyCityUser;

/**
 * ServiceManager keeps track of all running background services and some managers. It determines order of updating for classes that are not listeners.
 * ServiceManager should be initialized after we successfully connect to GTalk, and then should be the only class which is updated
 * in the background thread (i.e.: it updates all of its children)
 * @author Ryan
 *
 */
public class ServiceManager {

	static ChatPacketService 	cps;
	static UserService 			us;
	static MyCoordService 		mcs;
	static XMPPConnection 		conn;
	static ChatService			cs;
	
	protected static boolean hasBeenInitialized = false;
	
	protected static boolean cpsInitialized = false;
	protected static boolean usInitialized = false;
	protected static boolean mcsInitialized = false;
	protected static boolean connInitialized = false;
	protected static boolean csInitialized = false;
	
	public void init(ChatPacketService cps, UserService us, MyCoordService mcs, XMPPConnection conn, ChatService cs)
	{
		ServiceManager.cps 	= cps;
		cpsInitialized 	= true;
		
		ServiceManager.us 	= us;
		usInitialized 		= true;
		
		ServiceManager.mcs 	= mcs;
		mcsInitialized 	= true;
		
		ServiceManager.conn = conn;
		connInitialized 	= true;
		
		ServiceManager.cs 	= cs;
		csInitialized		= true;
		
		//Register some listeners which are dependent on one another here
		cps.registerListener(us);
		cps.registerListener(cs);
		hasBeenInitialized = true;
		
		//Set MCS's current user
		mcs.setMe(us.getMe());
	}
	
	public void updateServices()
	{
		cps.update();
		us.update();
		mcs.update();
		cs.update();
	}

	public static ChatPacketService getChatPacketService() {
		if(cpsInitialized)
			return cps;
		else
			throw new IllegalStateException();
	}

	public static UserService getUserService() {
		if(usInitialized)
			return us;
		else
			throw new IllegalStateException();
	}

	public static MyCoordService getMyCoordService() {
		if(mcsInitialized)
			return mcs;
		else
			throw new IllegalStateException();
	}
	
	public static ChatService getChatService() {
		if(csInitialized)
			return cs;
		else
			throw new IllegalStateException();
	}

	public static XMPPConnection getConnection() {
		
		return conn;
	}
}
