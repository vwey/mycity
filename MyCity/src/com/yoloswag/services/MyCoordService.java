package com.yoloswag.services;

import java.util.ArrayList;
import java.util.List;

import org.jivesoftware.smack.RosterEntry;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.packet.Presence;

import android.app.Activity;
import android.location.Location;
import android.location.LocationManager;
import android.util.Log;

import com.google.android.maps.GeoPoint;
import com.yoloswag.models.MyCityUser;
import com.yoloswag.services.interfaces.YoloswagListener;
import com.yoloswag.services.interfaces.YoloswagService;
import com.yoloswag.util.CoordParserReverse;

/**
 * Responsibility: Manages location and status for yourself.
 * @author cs110wau
 *
 */

public class MyCoordService implements YoloswagService {
	
	Activity dummyActivity = new Activity();
	ArrayList<MyCityUser> users;
	private LocationManager locationManager;
	private Location currentLocation;
	private GeoPoint currentPoint;
	private MyCityUser me;
	private String username;
	private List<String> providers;
	private XMPPConnection connection;
	
	public MyCoordService(String username, List<String> providers, LocationManager lm, XMPPConnection connection)
	{
		this.username = username;
		this.providers = providers;
		this.locationManager = lm;
		this.connection = connection;
	}
	
	@Override
	public void registerListener(YoloswagListener l) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void removeListener(YoloswagListener l) {
		throw new UnsupportedOperationException();
	}

	/**
	 * Payload:
	 * 
	 * UserLocation that represents your current location.
	 */
	@Override
	public void updateListeners() {
		throw new UnsupportedOperationException();
	}

	@Override
	public void update() {
		Log.d("MyCoordService", "Sending out coordinates");
		sendYoloswagStatus();
		getCurrentLocation();
		users = ServiceManager.getUserService().getBuddies();
		if(me != null)
		{
			Presence presence;
			for(MyCityUser user : users)
			{
				presence = connection.getRoster().getPresence(user.getJid());
				if(presence != null)
				{
					String status = presence.getStatus();
					if(status != null && presence.getStatus().equals("yoloswag"))
					{
						Log.v("MyCoordService", "Sending coordinates to " + user.getJid());
						ServiceManager.getChatPacketService().sendMessage(CoordParserReverse.parse(me)[0],user.getJid());
					}
				}
			}
		}
	}

	public void getCurrentLocation(){
		for(String provider : providers)
		{
		    currentLocation = locationManager.getLastKnownLocation(provider);
		    if(currentLocation != null){
		        setCurrentLocation(currentLocation);
		        break;
		    }
		}
	}

	public void setCurrentLocation(Location location){
	    int currLatitude = (int) (location.getLatitude()*1E6);
	    int currLongitude = (int) (location.getLongitude()*1E6);
	    currentPoint = new GeoPoint(currLatitude,currLongitude);
	    currentLocation = new Location("");
	    currentLocation.setLatitude(currentPoint.getLatitudeE6() / 1e6);
	    currentLocation.setLongitude(currentPoint.getLongitudeE6() / 1e6);
	    me.setLocation(currentLocation);
	}
	
 	public void sendYoloswagStatus()
 	{
        Presence presence = new Presence(Presence.Type.available);
        presence.setStatus("yoloswag");
        connection.sendPacket(presence);
 	}
 	
 	public void setMe(MyCityUser user)
 	{
 		this.me = user;
 	}
}
