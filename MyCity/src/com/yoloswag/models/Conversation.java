package com.yoloswag.models;

import java.util.ArrayList;
import java.util.Iterator;

import org.jivesoftware.smack.PacketListener;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smack.util.StringUtils;
import org.jivesoftware.smackx.muc.MultiUserChat;

import android.location.Location;
import android.util.Log;

import com.yoloswag.chat.ChatTypeEnum;
import com.yoloswag.services.ServiceManager;
import com.yoloswag.services.UserService;
import com.yoloswag.util.NotificationsManager;

/**
 * Conversation{} Contract
 * pre-conditions: none.
 * post-conditions: Manages all conversations (groups of messages) in the app
 * 					of a certain user.
 * 					Separates single user chats from multi user chats.
 * invariant: Messages within the conversations.
 */
public class Conversation implements PacketListener{
	ArrayList<Message> messages;
	ArrayList<MyCityUser> participants;
	ArrayList<MyCityUser> owners;
	
	ChatTypeEnum type;
	
	MultiUserChat chatroom;
	
	/**
	 * Conversation(MyCityUser user) Contract
	 * pre-conditions: There are no pre-existing conversations with this user
	 * post-conditions: messages is instantiated as an empty ArrayList. user is 
	 * 					added to the list of participants.
	 * invariants: none.
	 */
	public Conversation(MyCityUser user)
	{
		type = ChatTypeEnum.SINGLE;
		
		messages = new ArrayList<Message>();
		participants = new ArrayList<MyCityUser>();
		participants.add(user);
		chatroom = null;
	}
	
	/**
	 * Conversation(MultiUserChat chatroom) Contract
	 * pre-conditions: There are no pre-existing multi chat conversations.
	 * post-conditions: messages is instantiated as an empty ArrayList.
	 * 					participants is instantiated as an empty ArrayList.
	 * 					this.chatroom is set to the chatroom parameters.
	 * invariants: none.
	 */
	public Conversation(MultiUserChat chatroom)
	{
		type = ChatTypeEnum.MULTI;
		messages = new ArrayList<Message>();
		participants = new ArrayList<MyCityUser>();
		owners = new ArrayList<MyCityUser>();
		
		Iterator<String> users = chatroom.getOccupants();
		
		while(users.hasNext())
		{
			String curUser = users.next();
			String nickname = StringUtils.parseResource(curUser);
			String[] split = nickname.split(",");
			if(split.length == 2)
			{
				if(split[1].equals("owner"))
				{
					owners.add(ServiceManager.getUserService().findUser(split[0]));
				}
			}
		}

		chatroom.addMessageListener(this);
		this.chatroom = chatroom;
	}
	
	/**
	 * addMessage(Message m) Contract
	 * pre-conditions: none.
	 * post-conditions: Adds a message to the messages field.
	 * invariants: participants, chatroom unchanged.
	 */
	public void addMessage(Message m)
	{
		messages.add(m);
	}
	
	/**
	 * getMessages() Contract
	 * pre-conditions: messages is not null.
	 * post-conditions: none.
	 * invariants: messages, participants, chatroom unchanged
	 */
	public ArrayList<Message> getMessages() 
	{
		return messages;
	}
	
	/**
	 * setMessages(ArrayList<Message> messages) Contract
	 * pre-conditions: parameter is not null
	 * post-conditions: messages is set to parameter
	 * invariants: participants, chatroom unchanged
	 */
	public void setMessages(ArrayList<Message> messages) 
	{
		this.messages = messages;
	}
	
	/**
	 * getParticipants() Contract
	 * pre-conditions: participants is not null
	 * post-conditions: none.
	 * invariants: messages, participants, chatroom unchanged.
	 */
	public ArrayList<MyCityUser> getParticipants() 
	{
		return participants;
	}
	
	/**
	 * setParticipants(ArrayList<MyCityUser> participants) Contract
	 * pre-conditions: participants parameter is not null
	 * post-conditions: participants is set to parameter
	 * invariants: messages, chatroom unchanged
	 */
	public void setParticipants(ArrayList<MyCityUser> participants) 
	{
		this.participants = participants;
	}

	/**
	 * getChatroom() Contract
	 * pre-conditions: chatroom is not null
	 * post-conditions: none
	 * invariants: messages, participants, chatroom unchanged
	 */
	public MultiUserChat getChatroom() 
	{
		return chatroom;
	}

	/**
	 * setChatroom(MultiUserChat chatroom) Contract
	 * pre-conditions: chatroom parameter is not null
	 * post-conditions: chatroom is set to parameter
	 * invariants: messages, participants not changed
	 */
	public void setChatroom(MultiUserChat chatroom) 
	{
		this.chatroom = chatroom;
	}

	/**
	 * processPacket(Packet arg0) Contract
	 * pre-conditions: A message is sent through a chat room.
	 * post-conditions: The message is processed as a Packet and wrapped as a message
	 * 					object
	 * invariants: messages, chatroom, participants remain unchanged.
	 */
	@Override
	public void processPacket(Packet arg0) {
		Log.d("Conversation", "Received group chat packet");
		org.jivesoftware.smack.packet.Message smackMessage = (org.jivesoftware.smack.packet.Message) arg0;
		Message m = new Message();
		UserService service = ServiceManager.getUserService();
		MyCityUser to 	= service.findUser(smackMessage.getTo());
		MyCityUser from = service.findUser(smackMessage.getFrom());
		if(to == null)
		{
			to = new MyCityUser(new Location(""), "", "");
		}
		if(from == null)
		{
			from = new MyCityUser(new Location(""), "", "");
		}
		m.setTo(to);
		m.setFrom(from);
		m.setBody(smackMessage.getBody());
		NotificationsManager.multiChatNotification(NotificationsManager.getContext(), 
				"a shout");
		this.messages.add(m);
		Log.d("Conversation", "Inserted message from group chat: " + m.getBody());
	}

	public ChatTypeEnum getType() {
		return type;
	}

	public ArrayList<MyCityUser> getOwners() {
		return owners;
	}

	public void setOwners(ArrayList<MyCityUser> owners) {
		this.owners = owners;
	}
}
