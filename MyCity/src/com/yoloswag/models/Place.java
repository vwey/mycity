package com.yoloswag.models;

import com.google.api.client.util.Key;

/**
 * class Place Contract
 * Pre-condition: No precondition for this class
 * Post-condition: Initializes the name and other fields of a given place.
 */
public class Place {
	 @Key
	    public String id;
	 
	    @Key
	    public String name;
	 
	    @Key
	    public String reference;
	 
	    @Key
	    public String icon;
	 
	    @Key
	    public String vicinity;
	 
	    @Key
	    public Geometry geometry;
	 
	    @Key
	    public String formatted_address;
	 
	    @Key
	    public String formatted_phone_number;
	 
	    @Override
	    public String toString() {
	        return name + " - " + id + " - " + reference;
	    }
	    /**
	     * class Geometry Contract
	     * Pre-condition: No precondition for this class
	     * Post-condition: Initializes the location
	     */
	    public static class Geometry
	    {
	        @Key
	        public Location location;
	    }
	    
	    /**
	     * class Location Location
	     * Pre-condition: No precondition for this class
	     * Post-condition: Initializes the latitude and longitude coordinates for our location.
	     */
	    public static class Location
	    {
	        @Key
	        public double lat;
	 
	        @Key
	        public double lng;
	    }
}