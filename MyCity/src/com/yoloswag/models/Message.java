package com.yoloswag.models;

import java.util.Date;

/**
 * Message{} Contract
 * pre-conditions: A message has been sent from one user to another.
 * post-conditions: A message object is created containing a to, from, body, and 
 * 					timestamp feild for the original message.
 * invariants: none
 */
public class Message {
	MyCityUser 	from;
	MyCityUser 	to;
	String		body;
	Date 		timestamp;
	
	/**
	 * Message(MyCityUser from, MyCityUser to, String body, Date timestamp) Contract
	 * pre-conditions: A message object hasn't already been created with the same
	 * 					parameters
	 * post-conditions: A new message object is created with these parameters
	 * invariants: none
	 */
	public Message(MyCityUser from, MyCityUser to, String body, Date timestamp){
		this.from = from;
		this.to = to;
		this.body = body;
		this.timestamp = timestamp;
	}
	
	public Message() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * getFrom() Contract
	 * pre-conditions: from is not null
	 * post-conditions: none
	 * invariants: to, from, body, timestamp remain unchanged
	 */
	public MyCityUser getFrom() 
	{
		return from;
	}
	
	/**
	 * setFrom(MyCityUser from) Contract
	 * pre-conditions: from parameter is not null
	 * post-conditions: from is set to parameter
	 * invariants: to, body, timestamp remain unchanged
	 */
	public void setFrom(MyCityUser from) 
	{
		this.from = from;
	}
	
	/**
	 * getTo() Contract
	 * pre-conditions: to is not null
	 * post-conditions: none
	 * invariants: to, from, body, timestamp remain unchanged
	 */
	public MyCityUser getTo() 
	{
		return to;
	}
	
	/**
	 * setTo(MyCityUser to) Contract
	 * pre-conditions: to parameter is not null
	 * post-conditions: to is set to parameter
	 * invariants: from, body, timestamp remain unchanged
	 */
	public void setTo(MyCityUser to) 
	{
		this.to = to;
	}
	
	/**
	 * getBody() Contract
	 * pre-conditions: body is not null
	 * post-conditions: none
	 * invariants: to, from, body, timestamp remain unchanged
	 */
	public String getBody() 
	{
		return body;
	}
	
	/**
	 * setBody(String body) Contract
	 * pre-conditions: body parameter is not null
	 * post-conditions: body is set to parameter
	 * invariants: to, from, timestamp remain unchanged
	 */
	public void setBody(String body)
	{
		this.body = body;
	}
	
	/**
	 * getTimestamp() Contract
	 * pre-conditions: timestamp is not null
	 * post-conditions: none
	 * invariants: to, from, body, timestamp remain unchanged
	 */
	public Date getTimestamp() 
	{
		return timestamp;
	}
	
	/**
	 * setTimestamp(Date timestamp) Contract
	 * pre-conditions: timestamp parameters is not null
	 * post-conditions: timestamp is set to the parameter
	 * invariants: to, from, body remain unchanged
	 */
	public void setTimestamp(Date timestamp) 
	{
		this.timestamp = timestamp;
	}
}
