package com.yoloswag.models;

import android.location.Location;

/**
 * class UserPin Contract
 * Pre-condition: UserPin implements MapObject. Map activity must be running.
 * Post-condition: Has methods that update the name, email, and locations
 */
public class UserPin implements MapObject {

	String subscrType;

	MyCityUser user;
	private String name;
	private String email;
	
	/**
	 * UserPin(String name, String email, MyCityUser u) Contract
	 * Pre-condition: Map activity must be running and must have name
	 * Post-condition: Name on map is updated with new name.
	 * @param newName
	 */
	public UserPin(String name, String email, MyCityUser u)
	{
		this.name = name;
		this.email = email;
		this.user = u;
	}
	
	/**
	 * updateLocation(double lat, double lon) Contract
	 * Pre-condition: Map activity must be running and must have name
	 * Post-condition: Pin latitude and longitude are updated on the map
	 * @param lat
	 * @param lon
	 * @return Location
	 */
	public Location updateLocation(double lat, double lon) {
		Location l = new Location("");
		l.setLatitude(lat);
		l.setLongitude(lon);
		
		user.setLocation(l);
		
        return l;
	}
	
	/**
	 * updateLocation(MyCityUser u) Contract
	 * Pre-condition: Map activity must be running and must have name
	 * Post-condition: Location is updated taking in parameter MyCityUser
	 * @param MyCityUser
	 * @return Location
	 */
	public Location updateLocation(MyCityUser u) {
		user = u;
		
        return u.getLocation();
	}
	
	/**
	 * updateName(String newName) Contract
	 * Pre-condition: Map activity must be running and must have name
	 * Post-condition: Name on map is updated with new name.
	 *
	 * @param newName
	 * @return String
	 */
	public String updateName(String newName){
		if(newName != null){
			name = newName;
			return name;
		}
		else
			return null;
	}
	
	/**
	 * updateEmail(String newEmail) Contract
	 * Pre-condition: Map activity must be running and must have name
	 * Post-condition: Old email is replaced with new email.
	 *
	 * @param newName
	 * @return String
	 */
	public String updateEmail(String newEmail){
		if(newEmail != null){
			email = newEmail;
			return email;
		}
		else
			return null;
	}

	/**
	 * getUser(String newEmail) Contract
	 * Pre-condition: Map activity must be running and must have name
	 * Post-condition: Retrieves the user in order to set the user location.
	 *
	 * @param newName
	 * @return String
	 */
	public MyCityUser getUser() {
		return user;
	}
	
	/**
	 * setUserLoc(MyCityUser u) Contract
	 * Pre-condition: Retrieve the user.
	 * Post-condition: Sets the user location.
	 *
	 * @param MyCityUser
	 * @return void
	 */
	public void setUserLoc(MyCityUser u) {
		this.user = u;
	}

	/**
	 * getName() Contract
	 * Pre-condition: Map activity must be running and must have name
	 * Post-condition: Retrieves the name of the user.
	 *
	 * @return String
	 */
	public String getName() {
		return name;
	}

	/**
	 * setName(String name) Contract
	 * Pre-condition: Retrieve the user and the name.
	 * Post-condition: Sets the name of the user.
	 *
	 * @param String
	 * @return void
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * getName() Contract
	 * Pre-condition: User must be retrieved and main activity must be running
	 * Post-condition: Gets the email of the user.
	 *
	 * @return String
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * setEmail(String email) Contract
	 * Pre-condition: User email must be retrieved, and main activity must be running
	 * Post-condition: Sets the email of the user.
	 *
	 * @param String
	 * @return void
	 */
	public void setEmail(String email) {
		this.email = email;
	}
}
