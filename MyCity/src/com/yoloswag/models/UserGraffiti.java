package com.yoloswag.models;

import android.location.Location;

/**
 * interface UserGraffiti Contract
 * Pre-condition: This class implements the MapObject interface.
 * Post-condition: Name and locations are updated
 */
public class UserGraffiti implements MapObject {
	private double latitude;
	private double longitude;


	/**
	 * interface updateLocation(double lat, double lon) Contract
	 * Pre-condition: Map activity must be running and must have current latitude and longitude set. 
	 * Post-condition: Latitude and longitude updated.
	 * @Override
	 */
	public Location updateLocation(double lat, double lon) {
		
		latitude = lat;
		longitude = lon;

		return null;
	}

	/**
	 * interface updateName(String newName) Contract
	 * Pre-condition: Map activity must be running and must have current 
	 * Post-condition: Updates the name.
	 * @Override
	 */
	public String updateName(String newName) {
		// TODO Auto-generated method stub
		return null;
	}

}
