package com.yoloswag.models;

import org.apache.http.client.HttpResponseException;
import android.util.Log;
import com.google.api.client.googleapis.GoogleHeaders;
import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestFactory;
import com.google.api.client.http.HttpRequestInitializer;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.http.json.JsonHttpParser;
import com.google.api.client.json.jackson.JacksonFactory;
/**
 * class GooglePlaces Contract
 * Pre-condition: No preconditions required for this class
 * Post-condition: Obtains information from Google API including lat, lon, radius, location, key, and sensor
 */
@SuppressWarnings("deprecation")
public class GooglePlaces {
 
    /** 
     * Global instance of the HTTP transport. 
     */
    private static final HttpTransport HTTP_TRANSPORT = new NetHttpTransport();
    
    // Google API Key
    private static final String API_KEY = "AIzaSyBWhE4gZwdnXJWSSbc-Pq3PhybIbWy15NA";
    
    // Google Places search url's
    private static final String PLACES_SEARCH_URL = "https://maps.googleapis.com/maps/api/place/search/json?";
    private double _latitude;
    private double _longitude;
    private double _radius;

	/**
	 * search(double latitude, double longitude, double radius, String types) Contract
	 * 
	 * Pre-condition: Main activity must be running
	 * Post-condition: Obtains the map key, location, radius, and sensor from the Google API 
	 *
     * @param latitude - latitude of place
     * @param longitude - longitude of place
     * @param radius - radius of searchable area
     * @param types - type of place to search
     * @return list of places
     * */
    public PlaceList search(double latitude, double longitude, double radius, String types)
            throws Exception {
 
        this._latitude = latitude;
        this._longitude = longitude;
        this._radius = radius;
        
        try {
            HttpRequestFactory httpRequestFactory = createRequestFactory(HTTP_TRANSPORT);
            HttpRequest request = httpRequestFactory
                    .buildGetRequest(new GenericUrl(PLACES_SEARCH_URL));
            request.getUrl().put("key", API_KEY);
            request.getUrl().put("location", _latitude + "," + _longitude);
            request.getUrl().put("radius", _radius); // in meters
            request.getUrl().put("sensor", "false");
            if(types != null)
                request.getUrl().put("types", types);
            
            PlaceList list = request.execute().parseAs(PlaceList.class);
            // Check log cat for places response status
            Log.d("Places Status", "" + list.status);
            return list;
            
        } catch (HttpResponseException e) {
            Log.e("Error:", e.getMessage());
            return null;
        }
    }
 
    /**
	 * createRequestFactory(final HttpTransport transport) Contract
	 * 
	 * Pre-condition: Main activity must be running 
	 * Post-condition: Makes an HTTP request, sets Google headers, and creates HTTP and json factories.
	 */
    public static HttpRequestFactory createRequestFactory(
            final HttpTransport transport) {
        return transport.createRequestFactory(new HttpRequestInitializer() {
            public void initialize(HttpRequest request) {
                GoogleHeaders headers = new GoogleHeaders();
                headers.setApplicationName("AndroidHive-Places-Test");
                request.setHeaders(headers);
                JsonHttpParser parser = new JsonHttpParser(new JacksonFactory());
                request.addParser(parser);
            }
        });
    }

}