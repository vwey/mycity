package com.yoloswag.models;

import org.jivesoftware.smack.util.StringUtils;

import android.location.Location;
import android.util.Log;

import com.google.android.maps.GeoPoint;

/**
 * MyCityUser{} Contract
 * Pre-condition: User must be retrieved and main activity must be running
 * Post-condition: Gets, sets, and updates MyCityUser fields.
 */
public class MyCityUser {
	private Location location;
	private final String jid; // Can only be set at instantiation. One object per UID.
	private String realName;
	//private Time updateTime;
	
	private GeoPoint geoPoint; // Generated, shouldn't be able to be modified
	
	/**
	 * MyCityUser(Location l, String jid, String realName) Contract
	 * Pre-condition: Address and real name must be retrieved.
	 * Post-condition: Parse the address taken in and set new location.
	 */
	public MyCityUser(Location l, String jid, String realName)
	{
		this.realName = realName;
		this.location = l;
		this.jid = StringUtils.parseBareAddress(jid);
		//this.updateTime = t;
		if(location != null)
		{
			this.geoPoint = new GeoPoint((int) (location.getLatitude()/(1E6)), (int) (location.getLongitude()/(1E6)));
		}
	}
	
	/**
	 * getGeoPoint() Contract
	 * Pre-condition: Geopoint must be retrieved and map activity must be running.
	 * Post-condition: Gets a GeoPoint object representing the location of this user
	 */
	public GeoPoint getGeoPoint()
	{
		return geoPoint;
	}
	
	/*
	 * Getters and Setters 
	 */
	/**
	 * getLocation() Contract
	 * Pre-condition: Location must be retrieved and map activity must be on.
	 * Post-condition: Returns the current location of this object
	 * @return Location
	 */
	public Location getLocation() {
		return location;
	}

	/**
	 * setLocation(Location location) contract
	 * Pre-condition: Location retrieved.
	 * Post-condition: Sets the current location of this object.
	 * @param location
	 */
	public void setLocation(Location location) {
		this.location = location;
		geoPoint = new GeoPoint((int) (location.getLatitude()*(1E6)), (int) (location.getLongitude()*(1E6)));
	}

	/**
	 * setLocation(double lat, double lon) Contract
	 * Pre-condition: Location must be retrieved.
	 * Post-condition: Sets the location using latitude and longitude
	 * @param lat
	 * @param lon
	 */
	public void setLocation(double lat, double lon)
	{
		Location newLoc = new Location("");
		newLoc.setLatitude(lat);
		newLoc.setLongitude(lon);
		this.location = newLoc;
		geoPoint = new GeoPoint((int) (location.getLatitude()*(1E6)), (int) (location.getLongitude()*(1E6)));
	}
	/**
	 * getJid() Contract
	 * Pre-condition: Main activity must be running.
	 * Post-condition: Returns the Jabber ID used to uniquely represent
	 * a user. 
	 * @return String
	 */
	public String getJid() {
		return jid;
	}

	/**
	 * getRealName() Contract
	 * Pre-condition: Main activity must be running.
	 * Post-condition: Gets the MyCityUser's real name.
	 * a user. 
	 * @return String
	 */
	public String getRealName() {
		return realName;
	}

	/**
	 * setRealName(String realName) Contract
	 * Pre-condition: Real name must be retrieved.
	 * Post-condition: Sets the real name for MyCityUser.
	 * a user. 
	 * @param String
	 */
	public void setRealName(String realName) {
		this.realName = realName;
	}

	/**
	 * equals(Object other) Contract
	 * Pre-condition: Main activity must be running.
	 * Post-condition: Returns the Jabber ID used to uniquely represent
	 * a user. 
	 * @param Object
	 * @return boolean
	 * @Override
	 */
	public boolean equals(Object other)
	{
		if(other == null)
			return false;
		MyCityUser othermcu = (MyCityUser) other;
		
		if(!(this.jid.equals(othermcu.jid)))
		{
			return false;
		}
		return true;
	}
				
}
