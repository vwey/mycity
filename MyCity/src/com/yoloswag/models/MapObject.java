package com.yoloswag.models;

import android.location.Location;
/**
 * interface MapObject Contract
 * Pre-condition: Map activity must be running and must have name.
 * Post-condition: Name and locations are updated.
 */
public interface MapObject {
	
	/**
	 * updateName(String newName) Contract
	 * Pre-condition: Map activity must be running and must have name
	 * Post-condition: Name on map is updated with new name.
	 *
	 * @param newName
	 * @return String
	 */
	public String updateName(String newName);
	
	/**
	 * Location updateLocation(double lat, double lon) Contract
	 * Pre-condition: Map activity must be running and must have current latitude and longitude set.
	 * Post-condition: Location on map is updated with new latitude and longitude.
	 */
	public Location updateLocation(double lat, double lon);
}
