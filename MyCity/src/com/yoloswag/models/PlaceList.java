package com.yoloswag.models;

import java.util.List;
import com.google.api.client.util.Key;

/**
 * class PlaceList Contract
 * Pre-condition: No precondition for this class
 * Post-condition: Initializes the status and the list of results for place
 */
public class PlaceList {
	 
    @Key
    public String status;
 
    @Key
    public List<Place> results;
 
}